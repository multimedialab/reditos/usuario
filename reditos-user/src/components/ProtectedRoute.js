import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import AppMenu from './AppMenu';

export default function ProtectedRoute({ component: Component, ...rest }) {
    const auth = atob(sessionStorage.getItem('ath'));
    return (
        <Route {...rest} render={props => (
            auth === 'true'?
            <AppMenu><Component {...props} /></AppMenu>
            : <Redirect to="/" />
        )} />
    )
}
