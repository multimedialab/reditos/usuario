import React, { useState, useEffect } from "react";
import "./styleDashboard.scss";
import logo from "../../assets/icons/logo.png";
import exit from "../../assets/icons/exit.png";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link, useLocation } from "react-router-dom";
import ls from "local-storage";

//logos
import profileLogo from "../../assets/icons/profileLogo.png";
import profileBlue from "../../assets/icons/profileBlue.png";
import personGroup from "../../assets/icons/personGroupLogo.png";
import dependants from "../../assets/icons/dependantsBlue.png";
import resumeBlue from "../../assets/icons/resumeBlue.png";
import resumenLogo from "../../assets/icons/ResumenLogo.png";
import followLogo from "../../assets/icons/follow.png";
import tracingBlue from "../../assets/icons/tracingBlue.png";
import conversationlog from "../../assets/icons/conversation2.png";
import conversationLogoBlue from "../../assets/icons/conversation.png";

export default function Dashboard(props) {
  const [changeColor, setChangeColor] = useState("profile");
  let location = useLocation();

  useEffect(() => {
    setChangeColor(location.pathname);
  }, [location]);

  const logout = () => {
    sessionStorage.clear();
    ls.remove("ld");
    ls.remove("ath");
    ls.remove("uld");
    ls.remove("profileImage");
  };

  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        variant="dark"
        style={{ backgroundColor: "#004F9E" }}
        className="header"
      >
        <Navbar.Brand href="/profile">
          <img src={logo} alt="logoReditos" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Link
              to="/profile"
              className="text-center link"
              style={changeColor === "/profile" ? { background: "white" } : {}}
            >
              <div
                to="/profile"
                style={{ color: "white" }}
                className="text-center mt-4"
              >
                <img
                  src={changeColor === "/profile" ? profileBlue : profileLogo}
                  alt="profile"
                />
                <div>
                  <p
                    className="mt-2"
                    style={
                      changeColor === "/profile" ? { color: "#28509e" } : {}
                    }
                  >
                    Perfil
                  </p>
                </div>
              </div>
            </Link>
            {atob(sessionStorage.getItem("ld")) === "true" ? (
              <Link
                to="/dependents"
                className="text-center link"
                style={
                  changeColor === "/dependents" ? { background: "white" } : {}
                }
              >
                <div
                  style={{ color: "white", padding: 7 }}
                  className=" text-center mt-4"
                >
                  <img
                    src={
                      changeColor === "/dependents" ? dependants : personGroup
                    }
                    alt="dependents"
                  />
                  <div>
                    <p
                      className="mt-2"
                      style={
                        changeColor === "/dependents"
                          ? { color: "#28509e" }
                          : {}
                      }
                    >
                      Colaboradores a cargo
                    </p>
                  </div>
                </div>
              </Link>
            ) : (
              <div></div>
            )}
            <Link
              to="/resume"
              className="text-center link"
              style={changeColor === "/resume" ? { background: "white" } : {}}
            >
              <div
                style={{ color: "white", padding: 5 }}
                className="text-center mt-4"
              >
                <img
                  src={changeColor === "/resume" ? resumeBlue : resumenLogo}
                  alt="logoresume"
                />
                <p
                  className="mt-1"
                  style={changeColor === "/resume" ? { color: "#28509e" } : {}}
                >
                  Resumen
                </p>
              </div>
            </Link>
            <Link
              to="/tracing"
              className="text-center link"
              style={changeColor === "/tracing" ? { background: "white" } : {}}
            >
              <div
                style={{ color: "white", padding: 13 }}
                className="text-center mt-4"
              >
                <img
                  src={changeColor === "/tracing" ? tracingBlue : followLogo}
                  alt="logoblue"
                />
                <p
                  className="mt-2"
                  style={changeColor === "/tracing" ? { color: "#28509e" } : {}}
                >
                  Seguimiento1
                </p>
              </div>
            </Link>
            <Link
              to="/tracing1"
              className="text-center link"
              style={changeColor === "/tracing1" ? { background: "white" } : {}}
            >
              <div
                style={{
                  background: "none",
                  border: "none",
                  color: "white",
                  padding: 13,
                }}
                className="text-center mt-4"
              >
                <img
                  src={changeColor === "/tracing1" ? tracingBlue : followLogo}
                  alt="logoblue2"
                />
                <p
                  className="mt-2"
                  style={
                    changeColor === "/tracing1" ? { color: "#28509e" } : {}
                  }
                >
                  Seguimiento2
                </p>
              </div>
            </Link>
            <Link
              to="/conversation"
              className="text-center link"
              style={
                changeColor === "/conversation" ||
                changeColor === "/conversation/sweet" ||
                changeColor === "/conversation/bitter"
                  ? { background: "white" }
                  : {}
              }
            >
              <div
                style={{
                  background: "none",
                  border: "none",
                  color: "white",
                  padding: 13,
                }}
                className="text-center mt-4"
              >
                <img
                  src={
                    changeColor === "/conversation" ||
                    changeColor === "/conversation/sweet" ||
                    changeColor === "/conversation/bitter"
                      ? conversationLogoBlue
                      : conversationlog
                  }
                  alt="logoblue2"
                  style={{ width: 54, height: 54 }}
                />
                <p
                  style={
                    changeColor === "/conversation" ||
                    changeColor === "/conversation/sweet" ||
                    changeColor === "/conversation/bitter"
                      ? { color: "#28509e" }
                      : {}
                  }
                >
                  Conversación de valor
                </p>
              </div>
            </Link>
          </Nav>
          <Nav>
            <Nav.Link
              eventKey={2}
              href="/"
              className="mt-4 text-center"
              onClick={logout}
            >
              <img src={exit} alt="" style={{}} />
              <p className="mt-1" style={{ color: "white", marginRight: 20 }}>
                Salir
              </p>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <main className="containerapp">{props.children}</main>
    </>
  );
}
