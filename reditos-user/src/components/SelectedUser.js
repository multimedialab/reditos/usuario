import React from 'react'
import lupa from '../assets/icons/lupa.png'

export default function SelectedUser(props) {
    return (
        <div className="row">
            <div className="col-md-2">
                <label>Nombre del colaborador</label>
            </div>
            <div className="col-md-3">
                <input type="text" disabled value={props.name!== null?props.name:''} className="form-control" style={{ backgroundColor: '#F7B820', border: 'none' }} />
            </div>
            <div className="col-md-1">
                <img
                    src={lupa}
                    style={{ height: "30px", width: "30px" }}
                    alt=""
                />
            </div>
            <div className="col-md-2">
                <label>Identificación del colaborador</label>
            </div>
            <div className="col-md-3">
                <input disabled type="text" value={props.idCol!== null?props.idCol:''} className="form-control" style={{ backgroundColor: '#F7B820', border: 'none' }} />
            </div>
            <div className="col-md-1">
                <img
                    src={lupa}
                    style={{ height: "30px", width: "30px" }}
                    alt=""
                />
            </div>
        </div>
    )
}
