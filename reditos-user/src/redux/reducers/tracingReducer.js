import {
    SET_DATA_LOGIN,
    IS_SAVE_INDICATORS
  } from "../constants/actions-types";
  
  const getInitialState = () => ({
    dataLogged: [],
    isSaveIndicators: false
  });
  
  const tracingReducer = (state = getInitialState(), action) => {
    switch (action.type) {
      case SET_DATA_LOGIN:
        return { ...state, dataLogged: action.dataLogged };
      case IS_SAVE_INDICATORS:
        return { ...state, isSaveIndicators: action.isSaveIndicators };
      default:
        return state;
    }
  };

  export default tracingReducer;
  