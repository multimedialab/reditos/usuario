import {
    SET_DATA_LOGIN
  } from "../constants/actions-types";
  
  const getInitialState = () => ({
    dataLogged: [],
  });
  
  const userReducer = (state = getInitialState(), action) => {
    switch (action.type) {
      case SET_DATA_LOGIN:
        return { ...state, dataLogged: action.dataLogged };
       
      default:
        return state;
    }
  };
  export default userReducer;
  