import {
  SET_ADDITION_LIST,
  SET_BITTER_LIST,
  SET_BITTER_LIST_LEADER,
  SET_BOARD_COLLABORATORS,
  SET_COLLABORATOR_ID,
  SET_DATA_COLLABORATOR,
  SET_PERIOD,
  SET_SWEET_LIST,
  SET_SWEET_LIST_LEADER,
  SET_TYPE_BOARD,
  SET_WE_RECOGNIZE_LIST,
  SET_YEAR_BOARD,
} from "../constants/actions-types";

const getInitialState = () => ({
  sweetList: [],
  bitterList: [],
  additionList: [],
  weRecognizeList: [],
  yearBoard: "",
  period: 0,
  boardData: {},
  boardCollaborators: [],
  dataCollaborator: [],
  sweetListLeader: [],
  bitterListLeader: [],
  typeBoard: '',
});

const boardReducer = (state = getInitialState(), action) => {
  switch (action.type) {
    case SET_SWEET_LIST:
      return { ...state, sweetList: action.sweetList };
    case SET_BITTER_LIST:
      return { ...state, bitterList: action.bitterList };
    case SET_ADDITION_LIST:
      return { ...state, additionList: action.additionList };
    case SET_WE_RECOGNIZE_LIST:
      return { ...state, weRecognizeList: action.weRecognizeList };
    case SET_YEAR_BOARD:
      return { ...state, yearBoard: action.yearBoard };
    case SET_COLLABORATOR_ID:
        return { ...state, boardData: action.boardData };
    case SET_BOARD_COLLABORATORS:
      return { ...state, boardCollaborators: action.boardCollaborators };
    case SET_DATA_COLLABORATOR:
      return {...state, dataCollaborator: action.dataCollaborator};
    case SET_PERIOD:
      return {...state, period: action.period};
    case SET_SWEET_LIST_LEADER:
      return {...state, sweetListLeader: action.sweetListLeader};
    case SET_BITTER_LIST_LEADER:
      return {...state, bitterListLeader: action.bitterListLeader};
    case SET_TYPE_BOARD:
      return {...state, typeBoard: action.typeBoard};
    default:
      return state;
  }
};
export default boardReducer;
