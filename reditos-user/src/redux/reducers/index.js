import {combineReducers} from 'redux';
import boardReducer from './boardReducer';
import userReducer from './userReducer';
import tracingReducer from './tracingReducer';

const rootReducer = combineReducers({
    boardReducer,
    userReducer,
    tracingReducer
});

export default rootReducer;