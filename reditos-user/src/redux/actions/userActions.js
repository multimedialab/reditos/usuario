import {
    SET_DATA_LOGIN,
  } from "../constants/actions-types";
  import axios from "axios";
  import { url, body } from "../../constants/auth";
  
  export const setDataLogged = (dataLogged) => ({
    type: SET_DATA_LOGIN,
    dataLogged,
  });
  
  //This method is not used yet
  export const setCollaboratorData = (year, period, id) => {
    return async (dispatch) => {
      try {
        const responseToken = await axios.post(url.urlToken, body, {
          headers: { "Content-Type": "application/json" },
        });
  
        const token = await responseToken.data.token;
  
        const resp = await axios.get(
          `${url.urlBase}/requiredBoard/?year=${year}&period=${period}&collaborator=${id}`,
          {
            headers: { Authorization: `JWT ${token}` },
          }
        );
        return resp.data;
      } catch (error) {
        console.log("Error in boardActions => getCollaborators", error.message);
      }
    };
  };

  export const getSettings = () => {
    return async (dispatch) => {
      try {
        const responseToken = await axios.post(url.urlToken, body, {
          headers: { "Content-Type": "application/json" },
        });
  
        const token = await responseToken.data.token;
  
        const resp = await axios.get(
          `${url.urlBase}/settings/`,
          {
            headers: { Authorization: `JWT ${token}` },
          }
        );
        console.log('resp.data', resp.data)
        return resp.data.results[0];
      } catch (error) {
        console.log("Error in boardActions => getCollaborators", error.message);
      }
    };
  };
  