import {
  SET_ADDITION_LIST,
  SET_BITTER_LIST,
  SET_BOARD_COLLABORATORS,
  SET_SWEET_LIST,
  SET_WE_RECOGNIZE_LIST,
  SET_YEAR_BOARD,
  SET_COLLABORATOR_ID,
  SET_DATA_COLLABORATOR,
  SET_PERIOD,
  SET_SWEET_LIST_LEADER,
  SET_BITTER_LIST_LEADER,
  SET_TYPE_BOARD,
} from "../constants/actions-types";
import axios from "axios";
import { url, body } from "../../constants/auth";

export const setSweetList = (sweetList) => ({
  type: SET_SWEET_LIST,
  sweetList,
});

export const setBitterList = (bitterList) => ({
  type: SET_BITTER_LIST,
  bitterList,
});

export const setAdditionList = (additionList) => ({
  type: SET_ADDITION_LIST,
  additionList,
});

export const setWeRecognizeList = (weRecognizeList) => ({
  type: SET_WE_RECOGNIZE_LIST,
  weRecognizeList,
});

export const setSweetListLeader = (sweetListLeader) => ({
  type: SET_SWEET_LIST_LEADER,
  sweetListLeader,
});

export const setBitterListLeader = (bitterListLeader) => ({
  type: SET_BITTER_LIST_LEADER,
  bitterListLeader,
});

export const setYearBoard = (yearBoard) => ({
  type: SET_YEAR_BOARD,
  yearBoard,
});

export const setBoard = (boardData) => ({
  type: SET_COLLABORATOR_ID,
  boardData,
});

export const setBoardCollaborators = (boardCollaborators) => ({
  type: SET_BOARD_COLLABORATORS,
  boardCollaborators,
});

export const setDataCollaborator = (dataCollaborator) => ({
  type: SET_DATA_COLLABORATOR,
  dataCollaborator,
});

export const setPeriodNumber = (period) => ({
  type: SET_PERIOD,
  period,
});

export const setTypeBoard = typeBoard => ({
  type: SET_TYPE_BOARD,
  typeBoard,
});

export const getCollaboratorBoard = (year, period, id) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;

      const resp = await axios.get(
        `${url.urlBase}/requiredBoard/?year=${year}&period=${period}&collaborator=${id}`,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      return resp.data;
    } catch (error) {
      console.log("Error in boardActions => getCollaborators", error.message);
    }
  };
};

export const getBoardCollaborators = (year, period, idLeader, checked) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;
      const resp = await axios.get(
        `${url.urlBase}/requiredBoard/?year=${year}&period=${period}&leader=${idLeader}&check=${checked}`,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      console.log(resp);
      if (resp.data.count > 0) {
        /* filter(x => x.id_leader === JSON.parse(atob(sessionStorage.getItem('uld')))) */
        console.log(resp.data.results);
        dispatch(setBoardCollaborators(resp.data.results));
        return resp.data.results;
      } else {
        dispatch(setBoardCollaborators([]));
        return [];
      }
    } catch (error) {
      /* alert('Error in boardActions => getCollaborators', error.message); */
      console.log("Error in boardActions => getCollaborators", error.message);
      /* throw Error(error); */
    }
  };
};

export const getBoardColUnlimited= (year, idLeader, checked) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;
      const resp = await axios.get(
        `${url.urlBase}/boards/?year=${year}&leader=${idLeader}&check=${checked}`,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      console.log('resp unlimited',resp);
      if (resp.data.count > 0) {
        /* filter(x => x.id_leader === JSON.parse(atob(sessionStorage.getItem('uld')))) */
        console.log(resp.data.results);
        dispatch(setBoardCollaborators(resp.data.results));
        return resp.data.results;
      } else {
        dispatch(setBoardCollaborators([]));
        return [];
      }
    } catch (error) {
      /* alert('Error in boardActions => getCollaborators', error.message); */
      console.log("Error in boardActions => getCollaborators", error.message);
      /* throw Error(error); */
    }
  };
};

export const searchColaborator = (
  year,
  period,
  idLeader,
  checked,
  searchName
) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;

      const resp = await axios.get(
        `${url.urlBase}/requiredBoard/?year=${year}&period=${period}&leader=${idLeader}&check=${checked}&search=${searchName}`,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      console.log(resp);
      if (resp.data.count > 0) {
        console.log(resp.data.results);
        dispatch(setBoardCollaborators(resp.data.results));
        return resp.data.results;
      } else {
        dispatch(setBoardCollaborators([]));
        return [];
      }
    } catch (error) {
      /* alert('Error in boardActions => getCollaborators', error.message); */
      console.log("Error in boardActions => getCollaborators", error.message);
      /* throw Error(error); */
    }
  };
};

export const updateBoard = (id, value) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      const resp = await axios.put(
        `${url.urlBase}/requiredBoard/${id}/`,
        value,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      if (resp.data.count > 0) {
        /* filter(x => x.id_leader === JSON.parse(atob(sessionStorage.getItem('uld')))) */
        dispatch(setBoardCollaborators(resp.data.results));
        return resp.data.results;
      } else {
        dispatch(setBoardCollaborators([]));
      }
    } catch (error) {
      /* alert('Error in boardActions => getCollaborators', error.message); */
      console.log("Error in boardActions => getCollaborators", error.message);
      /* throw Error(error); */
    }
  };
};

export const updateBoardUnlimited = (id, value) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      const resp = await axios.put(
        `${url.urlBase}/boards/${id}/`,
        value,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      if (resp.data.count > 0) {
        /* filter(x => x.id_leader === JSON.parse(atob(sessionStorage.getItem('uld')))) */
        dispatch(setBoardCollaborators(resp.data.results));
        return resp.data.results;
      } else {
        dispatch(setBoardCollaborators([]));
      }
    } catch (error) {
      /* alert('Error in boardActions => getCollaborators', error.message); */
      console.log("Error in boardActions => getCollaborators", error.message);
      /* throw Error(error); */
    }
  };
};



export const createBoard = (obj) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      const resp = await axios.post(`${url.urlBase}/requiredBoard/`, obj, {
        headers: { Authorization: `JWT ${token}` },
      });
      if (resp.data.count > 0) {
        dispatch(setBoardCollaborators(resp.data.results));
        return resp.data.results;
      } else {
        dispatch(setBoardCollaborators([]));
      }
    } catch (error) {
      /* alert('Error in boardActions => getCollaborators', error.message); */
      console.log("Error in boardActions => getCollaborators", error.message);
      /* throw Error(error); */
    }
  };
};

export const createBoardUnlimited = (obj) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      const resp = await axios.post(`${url.urlBase}/boards/`, obj, {
        headers: { Authorization: `JWT ${token}` },
      });
      if (resp.data.count > 0) {
        dispatch(setBoardCollaborators(resp.data.results));
        return resp.data.results;
      } else {
        dispatch(setBoardCollaborators([]));
      }
    } catch (error) {
      /* alert('Error in boardActions => getCollaborators', error.message); */
      console.log("Error in boardActions => getCollaborators", error.message);
      /* throw Error(error); */
    }
  };
};



export const createSweetCard = (obj) => {
  return async (dispatch) => {
    try {
      const respToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await respToken.data.token;

      const data = await axios.post(`${url.urlBase}/sweet/`, obj, {
        headers: { Authorization: `JWT ${token}` },
      });

      console.log("data => ", data);
    } catch (error) {
      console.log("error => ", error.message);
      throw Error(error);
    }
  };
};

export const createBitterCard = (obj) => {
  return async (dispatch) => {
    try {
      const respToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await respToken.data.token;

      const data = await axios.post(`${url.urlBase}/bitter/`, obj, {
        headers: { Authorization: `JWT ${token}` },
      });

      console.log("data => ", data);
    } catch (error) {
      console.log(error.message);
    }
  };
};
