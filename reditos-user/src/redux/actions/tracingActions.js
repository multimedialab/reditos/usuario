import {
  SET_SWEET_LIST,
  IS_SAVE_INDICATORS
} from "../constants/actions-types";
import axios from "axios";
import { url, body } from "../../constants/auth";
import { formatDateString } from '../../utils/formatDate';

export const setSweetList = (sweetList) => ({
  type: SET_SWEET_LIST,
  sweetList,
});

export const setSaveIndi = (isSaveIndicators) => ({
  type: IS_SAVE_INDICATORS,
  isSaveIndicators
});

export const getTracingData = (idColab, initDate, endDate) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;

      const resp = await axios.get(
        `${url.urlBase}/tracing/?collaborator=${idColab}&initDate=${initDate}&endDate=${endDate}`,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      return resp.data.results[0];
    } catch (error) {
      console.log("Error in boardActions => getCollaborators", error.message);
    }
  };
};

export const updateTracingIndicator = (data, strenghts, improvements) => {
  return async (dispatch) => {
    try {
      let isSave = false;
      if (data.length !== 0) {
        const responseToken = await axios.post(url.urlToken, body, {
          headers: { "Content-Type": "application/json" },
        });
        const token = await responseToken.data.token;

        await data.forEach((item, index) => {
          let obj = {
            concept: item.concept,
            strengths: strenghts,
            improveChange: improvements,
            idItem: item.idItem,
            idTracing: item.idTracing
          }
          axios.put(`${url.urlBase}/tracingIndicators/${item.id}/`, obj, {
            headers: { Authorization: `JWT ${token}` },
          });

          if ((data.length - 1) === index) {
            isSave = true;
          }
        });
        return isSave;
      }
    } catch (error) {
      console.log("Error in boardActions => getCollaborators", error.message);
      return false;
    }
  };
};


export const createTracing = (obj, date) => {
  return async (dispatch) => {
    try {
      let idCol = obj.collaborator.split('/')[5];
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      const resp = await axios.get(`${url.urlBase}/tracing/?collaborator=${idCol}&initDate=${date}&endDate=${date}`,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
        console.log('resp.data.results[0].tracingItemsList.length', resp.data.results)
      if (resp.data.results.length === 0) {
        const tokenT = await responseToken.data.token;
        const respTracing = await axios.post(`${url.urlBase}/tracing/`, obj,
          {
            headers: { Authorization: `JWT ${tokenT}` },
          }
        );
        return {
          status: true,
          data: respTracing.data
        }
      } else if (resp.data.results[0].tracingItemsList.length === 0) {
        console.log('true elsefi')
        return {
          status: true,
          data: resp.data.results[0]
        }
      } else {
        console.log('false elsefi')
        return {
          status: false,
          data: []
        }
      }
    } catch (error) {
      console.log("Error in boardActions => getCollaborators", error.message);
    }
  };
};

export const saveTracingIndicator = (obj, tracing, weigth) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;

      let responsePromise = false;
      await obj.forEach((item, index) => {
        let indicator = {
          weigth: weigth,
          idItem: item.url,
          idTracing: tracing,
          concept: "",
          strengths: "",
          improveChange: ""
        }

        axios.post(`${url.urlBase}/tracingIndicators/`, indicator,
          {
            headers: { Authorization: `JWT ${token}` },
          }
        );

        if (index === obj.length - 1) {
          responsePromise = true;
        }
      });

      return responsePromise;

    } catch (error) {
      console.log('entre al true', error.message)
    }
  }
}

export const createAnswersChecklist = (obj, id, period) => {
  return async (dispatch) => {
    try {
      let answer = false;
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;
      const year = new Date().getFullYear()
      await obj.forEach((element, i) => {
        let newObj = {
          "name_question": element.name,
          "isChecked": element.isChecked,
          "id_question": element.url,
          "id_collaborator": id,
          "year": year,
          "period": period,
        }
        axios.post(
          `${url.urlBase}/answersChecklist/`, newObj,
          {
            headers: { Authorization: `JWT ${token}` },
          }
        );
        if (i === obj.length - 1) {
          answer = true
        }
      });
      return answer
    } catch (error) {
      console.log("Error in createAnswersChecklist =>", error.message);
    }
  };
};

export const checklistExist = (id, year, period) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      const resp = await axios.get(
        `${url.urlBase}/answersChecklist/?collaborator=${id}&year=${year}&period=${period}`,
        {
          headers: { Authorization: `JWT ${token}` },
        }
      );
      return resp.data

    } catch (error) {
      console.log("Error in checklist exist => ", error.message);
    }
  };
};

export const finishedTracing = (obj) => {
  return async (dispatch) => {
    try {
      let tracingUpdate = {
        collaborator: obj.collaborator,
        leader: obj.leader,
        finished: true,
        blockDate: formatDateString(new Date()),
        blockReason: 'El lider ha finalizado el proceso de revisión del seguimiento.'
      }

      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      const resp = await axios.put(`${url.urlBase}/tracing/${obj.id}/`, tracingUpdate, {
        headers: { Authorization: `JWT ${token}` },
      }
      );
      console.log('resp tracing actions', resp)
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
}

export const showChecklistFinished = (obj) => {
  return async (dispatch) => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;
      const resp = await axios.get(`${url.urlBase}/tracing/?collaborator=${obj.id}&year=${obj.year}&period=${obj.period}&finished=True`, {
        headers: { Authorization: `JWT ${token}` },
      }
      );
      console.log('resp.data from showchecklist', resp.data)
      return resp.data.count;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
}
