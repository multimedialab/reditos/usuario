import React, { useEffect, useState, useCallback } from "react";
import { useDispatch } from "react-redux";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import PlusIcon from "../assets/plus.png";
import TalkIcon from "../assets/conversemos.png";
import { AiFillBell } from "react-icons/ai";
import "./styles.css";
import { Modal, Button, Form } from "react-bootstrap";
import swal from "sweetalert2";
import { sendEmails } from "../services/notifications";
import {
  getBoardCollaborators,
  setSweetList,
  setBitterList,
  setBoard,
  getBoardColUnlimited,
} from "../redux/actions/boardActions";

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  console.log("list", list);
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);
  destClone.splice(droppableDestination.index, 0, removed);
  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;
  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  border: "2px solid rgb(39 80 158)",
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  // styles we need to apply on draggables
  ...draggableStyle,
});

const getItemStyle2 = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  /* border: "2px solid black", */
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "transparent",
  padding: grid,
  width: 250,
  borderRadius: 17,
});

const getListStyle2 = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "#ccc",
  padding: grid,
  width: "89%",
  borderRadius: 17,
  justifyContent: "center",
});

const SelectBoard = (props) => {
  const dispatch = useDispatch();
  const [state, setstate] = useState([]);
  const [selected, setSelected] = useState([]);
  const [board3, setBoard3] = useState([]);
  const [colabName, setColabName] = useState("");
  let objImages = [
    {
      id: 14,
      emoji: PlusIcon,
      plus: false,
    },
    {
      id: 15,
      emoji: TalkIcon,
      plus: true,
    },
  ];
  const [boardEmoji, setBoardEmoji] = useState(objImages);
  const [colorBoard, setColorBoard] = useState("#00969A");
  const [valuesDate, setValuesDate] = useState("");
  const [dateNotification, setDateNotification] = useState("");
  const [show, setShow] = useState(false);
  const [sweetListView, setSweetListView] = useState([]);
  const [bitterListView, setBitterListView] = useState([]);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const getListStyle3 = (isDraggingOver) => ({
    background: isDraggingOver ? "lightblue" : colorBoard,
    padding: grid,
    width: "89%",
    borderRadius: 17,
    justifyContent: "center",
  });

  const getDataCollaborators = useCallback(async () => {
    let idLead = JSON.parse(atob(sessionStorage.getItem("uld")));
    const idLeader = idLead.split("/")[5];
    let dateSystem = new Date();
    let month = dateSystem.getMonth() + 1;
    let period = 0;
    if (month >= 1 && month <= 6) {
      period = 1;
    } else {
      period = 2;
    }
    const year = new Date().getFullYear();
    await dispatch(getBoardCollaborators(year, period, idLeader, "False")).then(
      (resp) => {
        setstate(resp);
      }
    );
  }, [dispatch]);

  const getDataByCollaborator = (item) => {
    console.log(item.id);
    if (item) {
      console.log("item que llega => ", item);
      dispatch(setBoard(item));
      setColabName(item.nameCollaborator);
      setSweetListView(JSON.parse(item.data).sweetData);
      setBitterListView(JSON.parse(item.data).bitterData);
    }
  };

  const getDataColabUnlimited = useCallback(async () => {
    let idLead = JSON.parse(atob(sessionStorage.getItem("uld")));
    const idLeader = idLead.split("/")[5];
    const year = new Date().getFullYear();
    await dispatch(getBoardColUnlimited(year, idLeader, "False")).then(
      (resp) => {
        setstate(resp);
      }
    );
  }, [dispatch]);

  useEffect(() => {
    const info = sessionStorage.getItem("typeBoard");
    console.log("info", info);
    if (info === "UNLIMITED") {
      getDataColabUnlimited();
    } else {
      getDataCollaborators();
    }
  }, [getDataCollaborators, getDataColabUnlimited]);

  const getList = (id) => {
    if (id === "droppable") {
      return state;
    } else {
      return selected;
    }
  };

  const onDragEnd = (result) => {
    const { source, destination } = result;

    // dropped outside the list
    if (!destination) {
      return;
    }
    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        getList(source.droppableId),
        source.index,
        destination.index
      );
      let states = { items };

      if (source.droppableId === "droppable2") {
        states = { selected: items };
        setSelected(states.selected);
      } else {
        setstate(states.items);
      }
    } else {
      const result = move(
        getList(source.droppableId),
        getList(destination.droppableId),
        source,
        destination
      );
      if (source.droppableId === "droppable3") {
        //setSelected2([]);
        setBoard3(result.droppable3);
      } else {
        if (result.droppable2.length !== 0) {
          getDataByCollaborator(result.droppable2[0]);
        } else {
          setBoard3([]);
        }
        setstate(result.droppable);
        setSelected(result.droppable2);
      }
    }
  };

  const handleOnClick = (name) => {
    if (name === "Lo Dulce") {
      setColorBoard("#00969A");
      setBoard3(sweetListView);
    } else {
      setColorBoard("#EF8400");
      setBoard3(bitterListView);
    }
  };

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity

  const anotherCards = () => {
    let completeBoard = false;
    for (let index = 0; index < sweetListView.length; index++) {
      const element = sweetListView[index];
      if (element.selectedPlus !== undefined) {
        completeBoard = true;
        for (let index = 0; index < bitterListView.length; index++) {
          const element = bitterListView[index];
          if (element.selectedPlus !== undefined) {
            completeBoard = true;
          } else {
            swal(
              "Oops!",
              `Por favor seleccione una opcion para Lo Amargo: '${element.name}'`,
              "error"
            );
            completeBoard = false;
            return completeBoard;
          }
        }
      } else {
        swal(
          "Oops!",
          `Por favor seleccione una opcion para Lo dulce: '${element.name}'`,
          "error"
        );
        completeBoard = false;
        return completeBoard;
      }
    }

    if (completeBoard) {
      dispatch(setSweetList(sweetListView));
      dispatch(setBitterList(bitterListView));
      swal({
        title: "Notificación",
        text:
          "¿Deseas agregar nuevas tarjetas en 'Lo Dulce' y 'Exploremos Lo Amargo'?",
        icon: "info",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          swal("Correcto! Tu conversación ha sido guardada!", {
            icon: "success",
          });
          props.history.push("/conversation/sweetLeader");
          window.scroll(0, 0);
        } else {
          /* swal("Tu conversacion no fue guardada"); */
          props.history.push("/conversation/additions");
        }
      });
    }
  };

  const handleChange = (e) => {
    setValuesDate(([e.target.name] = e.target.value));
  };

  const convertStringToEmails = async () => {
    var expresionRegular = /\s*;\s*/;
    var listaDeCorreos = valuesDate.split(expresionRegular);
    var dateFormat = dateNotification.split("-").reverse().join("-");
    let obj = {
      emails: listaDeCorreos,
      date: dateFormat,
      subject: "",
      type: 1,
    };
    await sendEmails(obj);
    handleClose();
  };

  const clickedEmoji = (
    item,
    emojiId,
    index,
    isSelectedPlus,
    isSelectedConversation
  ) => {
    const newItem = {
      ...item,
      selectedPlus: isSelectedPlus,
      idEmoji: emojiId,
      selectedConversation: isSelectedConversation,
    };
    board3[index] = newItem;
    let objImages = [
      {
        id: 14,
        emoji: PlusIcon,
        plus: false,
      },
      {
        id: 15,
        emoji: TalkIcon,
        plus: true,
      },
    ];

    setBoardEmoji(objImages);
  };

  return (
    <>
      <div className="ml-5 mr-5">
        <h1
          style={{
            textAlign: "center",
            fontSize: 58,
            color: "#272727",
            fontWeight: "bold",
          }}
          className="resume"
        >
          Seleccionar Tablero
        </h1>

        <div
          style={{
            backgroundColor: "transparent",
            borderRadius: 50,
            border: "2px solid",
            borderColor: "#004F9E",
            padding: 50,
          }}
        >
          <AiFillBell
            onClick={() => handleShow()}
            className="bellIcon"
            color="#004F9E"
            size={30}
          />
          {colabName && (
            <h6 style={{ textAlign: "center" }}>
              CONVERSANDO CON{" "}
              <span style={{ color: "#27509e" }}>{colabName}</span>
            </h6>
          )}
          <hr />
          <DragDropContext onDragEnd={onDragEnd}>
            <div className="d-flex flex-row">
              <div>
                <input
                  className="form-control"
                  style={{ width: 250 }}
                  placeholder="Buscar"
                />
                <Droppable droppableId="droppable">
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      style={getListStyle(snapshot.isDraggingOver)}
                    >
                      {state.map((item, index) => (
                        <Draggable
                          key={item.id}
                          draggableId={item.id.toString()}
                          index={index}
                        >
                          {(provided, snapshot) => (
                            <div
                              onClick={() => handleOnClick()}
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={getItemStyle(
                                snapshot.isDragging,
                                provided.draggableProps.style
                              )}
                            >
                              {item.nameCollaborator}
                            </div>
                          )}
                        </Draggable>
                      ))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </div>
              <Droppable droppableId="droppable2">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle2(snapshot.isDraggingOver)}
                    className="ml-5"
                  >
                    {selected !== undefined ? (
                      <>
                        {selected.map((item, index) => (
                          <Draggable
                            key={item.id}
                            draggableId={item.id.toString()}
                            index={index}
                          >
                            {(provided, snapshot) => (
                              <>
                                <div
                                  ref={provided.innerRef}
                                  onClick={() => handleOnClick("Lo Dulce")}
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                  className=""
                                  style={getItemStyle2(
                                    snapshot.isDragging,
                                    provided.draggableProps.style
                                  )}
                                >
                                  Lo Dulce
                                </div>
                                <div
                                  ref={provided.innerRef}
                                  onClick={() => handleOnClick("Lo Amargo")}
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                  className=""
                                  style={getItemStyle2(
                                    snapshot.isDragging,
                                    provided.draggableProps.style
                                  )}
                                >
                                  Lo Amargo
                                </div>
                              </>
                            )}
                          </Draggable>
                        ))}
                      </>
                    ) : null}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>

              <Droppable droppableId="droppable3" isDropDisabled={true}>
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle3(snapshot.isDraggingOver)}
                    className="ml-5"
                  >
                    {board3 !== undefined ? (
                      <>
                        {board3.map((item, index) => (
                          <Draggable
                            key={item.id}
                            draggableId={item.id.toString()}
                            index={index}
                            isDragDisabled
                          >
                            {(provided, snapshot) => (
                              <div
                                ref={provided.innerRef}
                                key={item.name}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                className=""
                                style={getItemStyle2(
                                  snapshot.isDragging,
                                  provided.draggableProps.style
                                )}
                              >
                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    width: "100%",
                                  }}
                                >
                                  <strong style={{ width: 110 }}>
                                    {item.name}
                                  </strong>
                                  {boardEmoji.map((itemses) =>
                                    itemses.id === item.idEmoji ? (
                                      <div
                                        key={item.name}
                                        style={{ cursor: "pointer" }}
                                      >
                                        {item.selectedPlus ? (
                                          <img
                                            style={{
                                              cursor: "pointer",
                                              width: 40,
                                              height: 40,
                                              objectFit: "contain",
                                              marginLeft: 10,
                                              filter: item.selectedPlus
                                                ? "grayscale(0%)"
                                                : "grayscale(100%)",
                                            }}
                                            onClick={() => {
                                              clickedEmoji(
                                                item,
                                                itemses.id,
                                                index,
                                                !item.selectedPlus,
                                                !item.selectedConversation
                                              );
                                            }}
                                            src={itemses.emoji}
                                            alt=""
                                          />
                                        ) : (
                                          <>
                                            {item.selectedConversation && (
                                              <img
                                                style={{
                                                  cursor: "pointer",
                                                  width: 40,
                                                  height: 40,
                                                  objectFit: "contain",
                                                  marginLeft: 10,
                                                  filter: item.selectedConversation
                                                    ? "grayscale(0%)"
                                                    : "grayscale(100%)",
                                                }}
                                                onClick={() => {
                                                  clickedEmoji(
                                                    item,
                                                    itemses.id,
                                                    index,
                                                    !item.selectedPlus,
                                                    !item.selectedConversation
                                                  );
                                                }}
                                                src={itemses.emoji}
                                                alt=""
                                              />
                                            )}
                                          </>
                                        )}
                                      </div>
                                    ) : (
                                      <div key={itemses.id}>
                                        <img
                                          style={{
                                            cursor: "pointer",
                                            width: 40,
                                            height: 40,
                                            objectFit: "contain",
                                            marginLeft: 10,
                                            filter: "grayscale(100%)",
                                          }}
                                          onClick={() => {
                                            itemses.id === 14
                                              ? clickedEmoji(
                                                  item,
                                                  itemses.id,
                                                  index,
                                                  true,
                                                  false
                                                )
                                              : clickedEmoji(
                                                  item,
                                                  itemses.id,
                                                  index,
                                                  false,
                                                  true
                                                );
                                          }}
                                          src={itemses.emoji}
                                          alt=""
                                        />
                                      </div>
                                    )
                                  )}
                                </div>
                              </div>
                            )}
                          </Draggable>
                        ))}
                      </>
                    ) : null}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </div>
          </DragDropContext>
        </div>

        <div className="text-center mt-3">
          <button
            onClick={() => {
              window.scroll(0, 0);
              anotherCards();
            }}
            className="btn btn-primary"
            /* style={{
            backgroundColor: "white",
            border: "2px solid rgb(0, 79, 158)",
          }} */
          >
            Guardar
          </button>
        </div>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Envio de Notificaciónes</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <textarea
              style={{ maxHeight: 100 }}
              onChange={handleChange}
              name="emails"
              id=""
              cols="50"
              rows="10"
            ></textarea>
          </Form>
          <Form>
            <input
              onChange={(e) => {
                setDateNotification(e.target.value);
              }}
              className="form-control"
              type="date"
              name=""
              id=""
            />
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancelar
          </Button>
          <Button variant="primary" onClick={convertStringToEmails}>
            Enviar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default SelectBoard;
