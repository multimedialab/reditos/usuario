import React, { useState, useEffect, useCallback } from "react";
//import MaterialTable from "material-table";
//import tableIcons from "../../constants/MaterialIcons";
import axios from "axios";
import { body, url } from "../../constants/auth";
import { isMobile } from "react-device-detect";
import { Table } from "react-bootstrap";
import swal from "sweetalert2";
import {
  updateTracingIndicator,
  saveTracingIndicator,
} from "../../redux/actions/tracingActions";
import { useDispatch } from "react-redux";

export default function Values(props) {
  const dispatch = useDispatch();
  const [infoValue, setInfoValue] = useState([]);
  const [selected, setSelected] = useState(false);
  const [nameValue, setNameValue] = useState("");
  const [dataIndicat, setDataIndicat] = useState([]);
  const [selectedItem, setSelectedItem] = useState([]);
  const [indicator, setIndicator] = useState(false);
  const [newWeight, setNewWeight] = useState("");
  const [totalWeight, setTotalWeight] = useState(0);
  const [idValue, setIdValue] = useState("");
  const [strenghts, setStrenghts] = useState("");
  const [improvements, setImprovements] = useState("");

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      console.log("error", error.message);
    }
  };

  const getValuesCategory = useCallback(async () => {
    const token = await getToken();
    fetch(`${url.urlBase}/values/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => setInfoValue(json.results))
      .catch((error) => console.error(error));
  }, []);

  useEffect(() => {
    props.data.forEach((item, index) => {
      if (props.isTracing) {
        if (props.tracing.find((el) => el.value === item.id)) {
          item.weight = props.tracing.find((el) => el.value === item.id).weigth;
        } else {
          item.weight = 0;
        }
      }
    });
    getValuesCategory();
  }, [getValuesCategory, props.data, props.isTracing, props.tracing]);

  const onSelectItem = (e, item) => {
    let selectedItems = selectedItem;

    let i = selectedItems.indexOf(item);
    if (i !== -1) {
      selectedItems.splice(i, 1);
    } else {
      selectedItems.push(item);
    }

    setSelectedItem([...selectedItems]);
  };

  const dataTracing = () => {
    if (newWeight.length !== 0 && selectedItem.length !== 0) {
      swal(
        "Advertencia!",
        `Estas seguro de guardar los indicadores para ${nameValue} ?`,
        "warning",
        {
          buttons: {
            cancel: "No",
            accept: "Si",
          },
        }
      ).then((value) => {
        let tempTotal = totalWeight + parseInt(newWeight);
        if (value === "accept" && tempTotal <= 100) {
          const trecingId = sessionStorage.getItem("idt");
          dispatch(
            saveTracingIndicator(selectedItem, trecingId, newWeight)
          ).then((resp) => {
            if (resp) {
              props.data.find((el) => el.id === idValue).weight = newWeight;
              swal(
                "Genial!",
                "Lo indicadores para esta valor fueron almacenados correctamente.",
                "success"
              ).then(() => {
                setDataIndicat([]);
                setNewWeight("");
                setTotalWeight(tempTotal);
              });
            }
          });
        } else {
          swal("Oops!", "El peso no puede exceder el 100%", "error");
        }
      });
    } else {
      swal(
        "Oops!",
        "Por favor selecciona los indicadores e ingresa el peso correspondiente.",
        "warning"
      );
    }
  };

  const handleSelectUpdate = (e, item) => {
    const value = e.target.value;
    item.concept = value;
    setDataIndicat([...dataIndicat]);
  };

  const saveEvaluation = () => {
    if (strenghts.length !== 0 && improvements.length !== 0) {
      dispatch(
        updateTracingIndicator(dataIndicat, strenghts, improvements)
      ).then((resp) => {
        if (resp) {
          dataIndicat[0].strengths = strenghts;
          dataIndicat[0].improveChange = improvements;
          swal(
            "Genial!",
            "La información se ha almacenado correctamente.",
            "success"
          );
        }
      });
    } else {
      swal(
        "Oops!",
        "Por favor ingresa las fortalezas y oportunidades de mejora.",
        "warning"
      );
    }
  };

  return (
    <div>
      <div>
        <div style={{ textAlign: "center" }}>
          <h5>Categoría {props.name}</h5>
        </div>
        <div className="row">
          <div className="col-md-12">
            {/* <MaterialTable
              title=""
              icons={tableIcons}
              columns={[
                { title: "Valor", field: "nameValue", editable: "never" },
                { title: "Peso", field: "weight" },
              ]}
              data={props.data}
              actions={[
                {
                  icon: tableIcons.View,
                  tooltip: "ver",
                  onClick: (event, rowData) => {
                    setNameValue(rowData.nameValue);
                    setIdValue(rowData.id);
                    setSelectedItem([]);
                    props.isTracing ? setIndicator(false) : setIndicator(true); //this changes between tracing and dependents views

                    if (props.isTracing) {
                      let indicatorsList = props.tracing.filter(
                        (x) => x.value === rowData.id
                      );
                      if (indicatorsList.length !== 0) {
                        setDataIndicat(indicatorsList);
                        setStrenghts(indicatorsList[0].strengths);
                        setImprovements(indicatorsList[0].improveChange);
                        setSelected(true);
                      } else {
                        swal(
                          "Oops!",
                          "Aun no se han seleccionado indicadores para este valor"
                        );
                        setSelected(false);
                      }
                    } else {
                      if (rowData.weight === 0) {
                        for (let index = 0; index < infoValue.length; index++) {
                          let selected = infoValue[index].itemsList.filter(
                            (x) => x.name_value === rowData.nameValue
                          );
                          if (selected.length > 0) {
                            setDataIndicat(
                              infoValue[index].itemsList.filter(
                                (x) => x.name_value === rowData.nameValue
                              )
                            );
                          }
                        }
                      } else {
                        setNameValue("");
                        swal(
                          "Oops!",
                          "Ya seleccionaste los indicadores para este valor",
                          "info"
                        );
                      }
                    }
                  },
                },
              ]}
              options={{
                actionsColumnIndex: -1,
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: "No hay registros para mostrar",
                  addTooltip: "Ajouter",
                  deleteTooltip: "Supprimer",
                  editTooltip: "Editar",
                  filterRow: {
                    filterTooltip: "Filtrer",
                  },
                  editRow: {
                    deleteText: "Voulez-vous supprimer cette ligne?",
                    cancelTooltip: "Cancelar",
                    saveTooltip: "Aceptar",
                  },
                },
                grouping: {
                  placeholder: "Tirer l'entête ...",
                  groupedBy: "Grouper par:",
                },
                header: {
                  actions: "Acciones",
                },
                pagination: {
                  labelDisplayedRows: "{from}-{to} de {count}",
                  labelRowsSelect: "lineas",
                  labelRowsPerPage: "lineas por página:",
                  firstAriaLabel: "Primer página",
                  firstTooltip: "Primer página",
                  previousAriaLabel: "Página anterior",
                  previousTooltip: "Página anterior",
                  nextAriaLabel: "Página siguiente",
                  nextTooltip: "Página siguiente",
                  lastAriaLabel: "Última página",
                  lastTooltip: "Última página",
                },
                toolbar: {
                  addRemoveColumns: "Ajouter ou supprimer des colonnes",
                  nRowsSelected: "{0} ligne(s) sélectionée(s)",
                  showColumnsTitle: "Voir les colonnes",
                  showColumnsAriaLabel: "Voir les colonnes",
                  searchTooltip: "Buscar",
                  searchPlaceholder: "Buscar",
                },
              }}
            /> */}

            {indicator === true ? (
              <>
                <div className="text-center">
                  <button
                    onClick={() => props.customFunctionBack()}
                    style={{
                      backgroundColor: "white",
                      border: "2px solid #004F9E",
                      marginTop: 10,
                    }}
                  >
                    Atras
                  </button>
                </div>

                <br />
                <Table striped bordered hover size="sm">
                  <thead>
                    <tr>
                      <th
                        colSpan="2"
                        className="text-center"
                        style={{ fontSize: 20 }}
                      >
                        {dataIndicat.length !== 0 && `Indicador ${nameValue}`}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {dataIndicat.map((item, i) => {
                      return (
                        <tr key={i}>
                          <td className="text-center">
                            <label htmlFor={item.name_item}>
                              {item.name_item}
                            </label>
                          </td>
                          <td>
                            <input
                              id={item.name_item}
                              checked={selectedItem.includes(item)}
                              onChange={(e) => onSelectItem(e, item)}
                              className="text-center"
                              type="checkbox"
                            />
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
                {dataIndicat.length !== 0 && (
                  <div className="d-flex justify-content-center align-items-center mb-4 mt-4">
                    <div className="align-self-center">
                      Asignar nuevo peso (%)
                    </div>
                    <input
                      value={newWeight}
                      onChange={(event) => setNewWeight(event.target.value)}
                      style={{
                        width: "30%",
                        borderColor: "#004F9E",
                        marginLeft: 15,
                      }}
                    />
                  </div>
                )}
              </>
            ) : (
              <div></div>
            )}
            {indicator === false ? (
              <div className="text-center" style={{ marginTop: 10 }}>
                <button
                  onClick={() => props.customFunctionBack()}
                  style={{
                    backgroundColor: "white",
                    border: "2px solid #004F9E",
                  }}
                >
                  categorías
                </button>
              </div>
            ) : (
              <div className="text-center">
                {dataIndicat.length !== 0 && (
                  <button
                    onClick={() => dataTracing()}
                    style={{
                      backgroundColor: "white",
                      border: "2px solid #004F9E",
                      borderRadius: 5,
                    }}
                  >
                    Guardar
                  </button>
                )}
              </div>
            )}
          </div>
        </div>
        {props.isTracing && selected ? (
          <div>
            <div className="d-flex justify-content-center mt-3">
              <div
                style={
                  isMobile
                    ? { border: "2px solid blue", width: "100%" }
                    : { border: "2px solid blue", width: "70%" }
                }
              >
                <div className="ml-3 mt-3">
                  <span>Valor {nameValue}</span>
                </div>
                <div className="row d-flex align-content-center ml-5 mt-3 mr-1">
                  <div className="col-md-3 d-flex align-items-center">
                    <span>Fortalezas</span>
                  </div>
                  <div className="col-md-8 d-flex align-items-center">
                    <input
                      className="form-control"
                      onChange={(e) => setStrenghts(e.target.value)}
                      style={{ border: "2px solid #004F9E" }}
                      value={strenghts}
                      disabled={props.finishedTracing}
                    />
                  </div>
                </div>
                <div className="row d-flex align-content-center ml-5 mr-1">
                  <div className="col-md-3 d-flex align-items-center mt-3">
                    <span>Oportunidades de mejora</span>
                  </div>
                  <div className="col-md-8 d-flex align-items-center">
                    <input
                      className="form-control"
                      onChange={(e) => setImprovements(e.target.value)}
                      style={{ border: "2px solid #004F9E" }}
                      value={improvements}
                      disabled={props.finishedTracing}
                    />
                  </div>
                </div>
                <div className="ml-5 mt-4 mr-1">
                  <div>
                    <span>Indicadores</span>
                  </div>
                  {dataIndicat.map((item, i) => {
                    return (
                      <div className="mt-2" key={i}>
                        <span>
                          {i + 1}. {item.itemName}
                        </span>
                        <div className="row d-flex align-content-center ml-5 mr-1 mt-3">
                          <div className="col-md-6 d-flex align-items-center justify-content-center">
                            <span>Concepto</span>
                          </div>
                          <div className="col-md-5 d-flex align-items-center">
                            <select
                              className="form-control"
                              id="exampleFormControlSelect1"
                              style={{ border: "2px solid #004F9E" }}
                              onChange={(e) => handleSelectUpdate(e, item)}
                              value={item.concept}
                              disabled={props.finishedTracing}
                            >
                              <option value="">Seleccione una opcion</option>
                              <option>Superior</option>
                              <option>Alto</option>
                              <option>Medio</option>
                              <option>Bajo</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
                <div className="mb-4 mt-2 d-flex align-items-center justify-content-center">
                  <button
                    onClick={() => {
                      saveEvaluation();
                    }}
                    className="btn btn-outline-secondary"
                    style={{
                      border: "2px solid #004F9E",
                      borderRadius: 5,
                    }}
                    disabled={props.finishedTracing}
                  >
                    Guardar
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </div>
  );
}
