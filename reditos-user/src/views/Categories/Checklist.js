import React, { useState, useEffect, useCallback } from "react";
import { body, url } from "../../constants/auth";
import axios from "axios";
import { isMobile } from "react-device-detect";
import {
  checklistExist,
  createAnswersChecklist,
  showChecklistFinished,
} from "../../redux/actions/tracingActions";
import { useDispatch } from "react-redux";
import swal from "sweetalert2";
import { getSettings } from "../../redux/actions/userActions";

export default function Checklist(props) {
  const [selectedItem, setSelectedItem] = useState([]);
  const [checkedItems, setCheckedItems] = useState([]);
  const [info, setInfo] = useState([]);
  const [isCalified, setIsCalified] = useState(false);
  const [checkExist, setCheckExist] = useState(false);
  const [showMessage, setShowMessage] = useState("");
  const [showButton, setShowButton] = useState(false);
  const dispatch = useDispatch();

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      console.log("error", error.message);
    }
  };

  const consult = useCallback(async () => {
    const token = await getToken();
    fetch(`${url.urlBase}/checklist/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        setInfo(json.results[0].itemsQuestions);
        console.log("object", json.results[0].itemsQuestions);
      })
      .catch((error) => console.error(error));
  }, []);

  const handleChange = (e, item) => {
    let selectItem = selectedItem;
    let index = selectedItem.indexOf(item);
    if (index !== -1) {
      selectItem.splice(index, 1);
    } else {
      selectItem.push(item);
    }
    setSelectedItem(selectItem);
    setCheckedItems({ ...checkedItems, [e.target.name]: e.target.checked });
  };

  const uploadCheckList = () => {
    console.log("checkeditems", selectedItem);
    info.forEach((element) => {
      let found = selectedItem.find((i) => i.id === element.id);
      if (found) {
        element.isChecked = true;
      } else {
        element.isChecked = false;
      }
    });
    const data = JSON.parse(atob(sessionStorage.getItem("rdt")));
    let period = 0;
    props.isTracing1 ? (period = 2) : (period = 1);
    dispatch(
      createAnswersChecklist(info, data.collaboratorData.url, period)
    ).then((res) => {
      if (res) {
        setCheckExist(true);
        swal(
          "Información",
          "Las respuestas del checklist fueron guradadas correctamente",
          "success"
        );
      }
    });
  };

  useEffect(() => {
    const data = JSON.parse(atob(sessionStorage.getItem("rdt")));
    dispatch(getSettings()).then((item) => {
      var today = new Date();
      let initDate = new Date(item.initDateTracingOne);
      let endDate = new Date(item.endDateTracingOne);
      let initDateTwo = new Date(item.initDateTracingTwo);
      let endDateTwo = new Date(item.endDateTracingTwo);

      if (!props.isTracing1) {
        if (initDate.getTime() < today && today < endDate.getTime()) {
          let obj = {
            id: data.collaboratorData.id,
            year: today.getFullYear(),
            period: 1,
          };
          dispatch(showChecklistFinished(obj)).then((res) => {
            if (res !== 0) {
              dispatch(
                checklistExist(data.collaboratorData.id, today.getFullYear(), 1)
              ).then((res) => {
                if (res.count !== 0) {
                  //ya fue calificado por el usuario
                  setCheckExist(true);
                } else {
                  setCheckExist(false);
                  //falta validar para mostrar dependiendo del seguimiento 1 o 2
                  if (props.isTracing1) {
                    setShowMessage(
                      "No hay informacion disponible para este periodo"
                    );
                    setShowButton(false);
                  } else {
                    consult();
                    setShowMessage("Checklist del proceso");
                    setShowButton(true);
                  }
                }
              });
            } else {
              setIsCalified(true);
            }
          });
        } else {
          setShowMessage("No hay informacion disponible para este periodo");
        }
      } else {
        if (initDateTwo.getTime() < today && today < endDateTwo.getTime()) {
          let obj = {
            id: data.collaboratorData.id,
            year: today.getFullYear(),
            period: 2,
          };
          dispatch(showChecklistFinished(obj)).then((res) => {
            if (res !== 0) {
              dispatch(
                checklistExist(data.collaboratorData.id, today.getFullYear(), 2)
              ).then((res) => {
                if (res.count !== 0) {
                  //ya fue calificado por el usuario
                  setCheckExist(true);
                } else {
                  setCheckExist(false);
                  if (props.isTracing1) {
                    consult();

                    setShowMessage("Checklist del proceso");
                    setShowButton(true);
                  } else {
                    setShowMessage(
                      "No hay informacion disponible para este periodo"
                    );
                    setShowButton(false);
                  }
                }
              });
            } else {
              setIsCalified(true);
            }
          });
        } else {
          setShowMessage("No hay informacion disponible para este periodo");
        }
      }
    });
  }, [consult, dispatch, props.isTracing1]);

  return (
    <div className="d-flex justify-content-center mt-4">
      {isCalified === false ? (
        checkExist === false ? (
          <div style={{ width: "83%" }}>
            <div className="text-center mt-4 mb-4">
              <h3 className="resume" style={{ fontSize: 45 }}>
                {showMessage}
              </h3>
            </div>
            <div
              style={{
                border: "2px solid black",
                width: "90%",
                margin: "0 auto",
                borderRadius: 15,
                backgroundColor: "white",
              }}
            >
              {info.map((item, i) => {
                return (
                  <div
                    key={i}
                    style={{
                      borderTop: "1px solid #bbbbbb",
                      alignItems: "center",
                      padding: "12px 0px 7px 7px",
                      width: "100%",
                      borderRadius: i === 0 ? 12 : 0,
                    }}
                    className={
                      isMobile
                        ? "col-md-12 d-flex align-content-center"
                        : "col-md-12 d-flex align-content-center"
                    }
                  >
                    <div>
                      <input
                        id={item.name}
                        onClick={(e) => handleChange(e, item)}
                        name={item.name}
                        className="text-center checkBoxStyle"
                        type="checkbox"
                      />
                    </div>
                    <div className="ml-2">
                      <label
                        style={{ cursor: "pointer", fontSize: 18 }}
                        htmlFor={item.name}
                      >
                        {i + 1}. {item.name}
                      </label>
                    </div>
                  </div>
                );
              })}
            </div>
            {showButton && (
              <div className="d-flex justify-content-center mt-5 mb-4">
                <button
                  className="btn btn-primary btn-lg"
                  onClick={() => {
                    uploadCheckList();
                    props.customFunction();
                  }}
                >
                  Guardar
                </button>
              </div>
            )}
          </div>
        ) : (
          <h3>Ya has diligenciado el checklist para este periodo</h3>
        )
      ) : (
        <h3>El seguimiento aún no ha sido calificado</h3>
      )}
    </div>
  );
}
