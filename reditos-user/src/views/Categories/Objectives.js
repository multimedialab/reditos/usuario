import React, { useEffect, useState, useCallback } from "react";
//import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
//import VisibilityIcon from "@material-ui/icons/Visibility";
import { Table } from "react-bootstrap";
//import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
//import HelpIcon from "@material-ui/icons/Help";
import { isMobile } from "react-device-detect";
import { body, url } from "../../constants/auth";
import axios from "axios";
import swal from "sweetalert2";
import { deleteValue } from "../../services/delete";
import { putInfo } from "../../services/post";
import { withRouter } from "react-router-dom";

function Objectives(props) {
  const [isTracing, setIsTracing] = useState(false);
  const [objSelected, setObjSelected] = useState(false);
  const [addPlan, setAddPlan] = useState(false);
  const [data, setData] = useState([]);
  const [dataPlan, setDataPlan] = useState([]);
  const [tactical, setTactical] = useState([]);
  const [idTracing, setIdTracing] = useState("");
  const initialState = {
    obj_estrategico: "",
    obj_tactico: "",
    que_espera: "",
    como_lo_logra: "",
    weight: "",
    collaborator: sessionStorage.getItem("url"),
  };
  const initialStateAnswer = {
    observations: "",
    state: false,
    action_plan: "",
  };
  const [namePlan, setNamePlan] = useState("");
  const [values, setValues] = useState(initialState);
  const [valuesAnswer, setValuesAnswer] = useState(initialStateAnswer);

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      console.log("error", error.message);
    }
  };

  const consult = useCallback(async () => {
    const token = await getToken();
    fetch(`${url.urlBase}/strategic/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log(json.results);
        setData(json.results);
      })
      .catch((error) => console.error(error));
  }, []);

  const consultPlan = useCallback(async () => {
    const token = await getToken();
    fetch(`${url.urlBase}/planIndividual/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        setDataPlan(
          json.results.filter(
            (x) => x.collaborator === sessionStorage.getItem("url")
          )
        );
        console.log(
          json.results.filter(
            (x) => x.collaborator === sessionStorage.getItem("url")
          )
        );
      })
      .catch((error) => console.error(error));
  }, [setDataPlan]);

  //OnChange Inputs
  const handleOnChange = (e) => {
    console.log(e.target.value);
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
    console.log(values);
  };

  const handleOnChangeAnswer = (e) => {
    console.log(e.target.value);
    setValuesAnswer({
      ...valuesAnswer,
      [e.target.name]: e.target.value,
    });
    console.log(valuesAnswer);
  };

  const handleOnChangeStrat = (e) => {
    let strategic = data.filter((x) => x.url === e.target.value);

    if (strategic.length > 0) {
      setValues({
        ...values,
        obj_estrategico: strategic[0].name,
      });
    }
    console.log(values);
    selectTactic(e.target.value);
  };

  //this select tactical objectives depending of strategicselected
  const selectTactic = (e) => {
    if (e !== "default") {
      for (let index = 0; index < data.length; index++) {
        let tactic = data[index].tacticalList.filter(
          (x) => x.strategicObj === e
        );
        if (tactic.length > 0) {
          setTactical(
            data[index].tacticalList.filter((x) => x.strategicObj === e)
          );
        }
      }
    }
    console.log(values);
  };

  useEffect(() => {
    consult();
    consultPlan();
    setIsTracing(props.isTracing);
  }, [consult, props.isTracing, consultPlan]);

  //Guardar info plan individual
  const handleSavePlan = async () => {
    if (values.weight.includes(".")) {
      swal("Advertencia!", "El peso debe ser número entero!", "warning");
    }
    if (
      values.obj_estrategico !== "" &&
      values.obj_tactico !== "" &&
      values.que_espera !== "" &&
      values.como_lo_logra !== "" &&
      values.weight !== ""
    ) {
      if (values.weight !== "") {
        var totalWeight = dataPlan.reduce(function (tot, arr) {
          // return the sum with previous value
          return tot + arr.weight;
          // set initial value as 0
        }, 0);
        var result = totalWeight + values.weight;
        if (result > 100) {
          swal(
            "Advertencia!",
            "El peso total debe ser menor a 100%!",
            "warning"
          );
        } else {
          const token = await getToken();
          const response = await axios.post(
            `${url.urlBase}/planIndividual/`,
            values,
            { headers: { Authorization: `JWT ${token}` } }
          );
          console.log(response);
          setValues({ ...initialState });
          consultPlan();
          swal("Bien hecho!", "Registro guardado!", "success");
        }
      }
    } else {
      swal("Advertencia!", "Todos los campos son obligatorios!", "warning");
    }
  };

  //this saves the plan on tracing
  const handleSavePlanSelected = async () => {
    console.log("valores==>", valuesAnswer);
    if (
      valuesAnswer.actionPlan !== "" &&
      valuesAnswer.observations !== "" &&
      valuesAnswer.state !== ""
    ) {
      putInfo("evaluationObjectives", {
        ...valuesAnswer,
        id_plan: idTracing,
        state: valuesAnswer.state === "true" ? true : false,
      });
      swal(
        "Información!",
        "El plan individual fue guardado correctamente",
        "success"
      );
      props.history.push("/tracing");
      props.customFunctionBack();
    } else {
      swal("Advertencia!", "Todos los campos son obligatorios!", "warning");
    }
  };

  //eliminar plan individual
  const deletePlan = (id) => {
    console.log(id);
    swal({
      title: "¿Está seguro de Eliminar el plan individual?",
      icon: "warning",
      buttons: ["No", "Si"],
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteValue("planIndividual", id);
        setDataPlan([]);
        setTimeout(() => {
          consultPlan();
        }, 1200);
        swal("Registro Eliminado!", {
          icon: "success",
        });
      } else {
        swal("Registro no eliminado");
      }
    });
  };

  return (
    <div>
      <>
        <h3 className="text-center">Categoría OBJETIVOS</h3>
        {props.isTracing ? (
          <div></div>
        ) : (
          <div className="row mb-3 mt-3 d-flex">
            <div className="align-self-center">
              <h6 className="mr-2">Plan individual</h6>
            </div>
            <div className="align-self-center mb-2">
              <button
                style={{ border: "none", background: "none" }}
                onClick={() => setAddPlan(true)}
              >
                {/*  <AddCircleOutlineIcon style={{ color: "green" }} /> */}
              </button>
            </div>
          </div>
        )}
        <Table striped bordered hover size="sm" responsive>
          <thead>
            <tr>
              <th className="text-center">Objetivo estratégico</th>
              <th className="text-center">Objetivo táctico</th>
              <th className="text-center">Plan individual</th>
              <th className="text-center">Pesos</th>
              <th className="text-center">Opciones</th>
            </tr>
          </thead>
          <tbody>
            {dataPlan.map((item, i) => {
              return (
                <tr key={i}>
                  <td className="text-center">{item.obj_estrategico}</td>
                  <td className="text-center">{item.obj_tactico}</td>
                  <td className="text-center">{`${item.que_espera} ${item.como_lo_logra}`}</td>
                  <td className="text-center">{item.weight}</td>
                  <td className="text-center">
                    <button
                      style={{ background: "none", border: "none" }}
                      onClick={() => {
                        if (props.isTracing) {
                          if (item.evaluationList.length === 0) {
                            setObjSelected(true);
                            console.log("item objective", item);
                            setNamePlan(
                              `${item.que_espera} ${item.como_lo_logra}`
                            );
                            setIdTracing(item.url);
                          } else {
                            swal(
                              "Información",
                              "El plan de accion al que intentas acceder ya fue diligenciado",
                              "warning"
                            );
                          }
                        } else {
                          deletePlan(item.id);
                        }
                      }}
                    >
                      {/*  {props.isTracing ? (
                        <VisibilityIcon />
                      ) : (
                        <DeleteOutlineIcon />
                      )} */}
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        {addPlan ? (
          <>
            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th colSpan="2" className="text-center">
                    Definición
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="text-center">Objetivo estratégico</td>
                  <td className="text-center">
                    <select
                      style={{ width: "50%" }}
                      onChange={handleOnChangeStrat}
                      name="obj_estrategico"
                    >
                      <option value="">Seleccione una opción</option>
                      {data.map((item) => {
                        return (
                          <option value={item.url} key={item.name}>
                            {item.name}
                          </option>
                        );
                      })}
                    </select>
                  </td>
                </tr>
                <tr>
                  <td className="text-center">Objetivo táctico</td>
                  <td className="text-center">
                    <select
                      style={{ width: "50%" }}
                      onChange={handleOnChange}
                      name="obj_tactico"
                    >
                      <option value="">Seleccione una opción</option>
                      {tactical.map((item) => {
                        return (
                          <option
                            value={item.name}
                            title={item.name}
                            key={item.name}
                          >
                            {item.name.replace(/(.{50})..+/, "$1…")}
                          </option>
                        );
                      })}
                    </select>
                    {/*  <HelpIcon
                      style={{
                        position: "absolute",
                        marginLeft: 7,
                        cursor: "pointer",
                        color: "#f1b81e",
                      }}
                      onClick={() =>
                        swal(
                          "Tip!",
                          "Poner el mouse sobre un objetivo táctico, mostrará la descripción completa!",
                          "info"
                        )
                      }
                    /> */}
                  </td>
                </tr>
                <tr>
                  <td className="text-center">¿Qué espera lograr?</td>
                  <td className="text-center">
                    <input
                      onChange={handleOnChange}
                      name="que_espera"
                      value={values.que_espera}
                      style={{ width: "50%" }}
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-center">¿Cómo lo va a lograr?</td>
                  <td className="text-center">
                    <input
                      onChange={handleOnChange}
                      name="como_lo_logra"
                      value={values.como_lo_logra}
                      style={{ width: "50%" }}
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-center">Peso (%)</td>
                  <td className="text-center">
                    <input
                      onChange={handleOnChange}
                      name="weight"
                      type="number"
                      value={values.weight}
                      style={{ width: "50%" }}
                    />
                  </td>
                </tr>
              </tbody>
            </Table>
            <div className="mb-4 mt-2 d-flex align-items-center justify-content-center">
              <button
                onClick={() => handleSavePlan()}
                style={{
                  backgroundColor: "white",
                  border: "2px solid #004F9E",
                  borderRadius: 5,
                }}
              >
                Guardar
              </button>
            </div>
          </>
        ) : (
          <div></div>
        )}
      </>
      {isTracing && objSelected ? (
        <div>
          <div style={{ textAlign: "center", marginTop: 20 }}>
            <h5>Categoría {props.name}</h5>
          </div>
          <div className="d-flex justify-content-center mt-3">
            <div
              style={
                isMobile
                  ? { border: "2px solid blue", width: "100%" }
                  : { border: "2px solid blue", width: "70%" }
              }
            >
              <div className="ml-3 mt-3">
                <span>Plan Individual {namePlan}</span>
              </div>
              <div className="row d-flex align-content-center ml-5 mr-1 mt-3">
                <div className="col-md-3 d-flex align-items-center">
                  <span>Observaciones</span>
                </div>
                <div className="col-md-8 d-flex align-items-center">
                  <input
                    name="observations"
                    onChange={handleOnChangeAnswer}
                    className="form-control"
                    style={{ border: "2px solid #004F9E" }}
                  />
                </div>
              </div>
              <div className="row d-flex align-content-center ml-5 mr-1 mt-3">
                <div className="col-md-3 d-flex align-items-center mt-3">
                  <span>Estado</span>
                </div>
                <div className="col-md-8 d-flex align-items-center">
                  <select
                    className="form-control"
                    id="exampleFormControlSelect1"
                    style={{ border: "2px solid #004F9E" }}
                    name="state"
                    onChange={handleOnChangeAnswer}
                  >
                    <option>Por favor selecciona un estado</option>
                    <option value={true}>Aprobado</option>
                    <option value={false}>Rechazado</option>
                  </select>
                </div>
              </div>
              <div className="row d-flex align-content-center ml-5 mr-1 mt-3">
                <div className="col-md-3 d-flex align-items-center">
                  <span>Plan de acción</span>
                </div>
                <div className="col-md-8 d-flex align-items-center">
                  <input
                    name="action_plan"
                    onChange={handleOnChangeAnswer}
                    className="form-control"
                    style={{ border: "2px solid #004F9E" }}
                  />
                </div>
              </div>

              <div className="mb-4 mt-2 d-flex align-items-center justify-content-center">
                <button
                  onClick={() => handleSavePlanSelected()}
                  style={{
                    backgroundColor: "white",
                    border: "2px solid #004F9E",
                    borderRadius: 5,
                  }}
                >
                  Guardar
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
}

export default withRouter(Objectives);
