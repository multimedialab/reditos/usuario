import React, { useState } from "react";
import { Table } from "react-bootstrap";
import { isMobile } from "react-device-detect";
//import MaterialTable from 'material-table';
//import tableIcons from "../../constants/MaterialIcons";
import Checkbox from "@material-ui/core/Checkbox";

export default function Competencies(props) {
  const [compSelected, setCompSelected] = useState(false);
  const [dataIndicat, setDataIndicat] = useState([]);
  const [nameSelected, setNameSelected] = useState("");
  const [state, setState] = useState({
    check: true,
  });

  const { check } = state;

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
    console.log(event.target.selected);
  };

  return (
    <div>
      <h3 className="text-center mt-4">Categoría COMPETENCIAS</h3>
      <h6 className="mt-3 mb-3">Lista de competencias transversales</h6>
      {/* <MaterialTable
        title=""
        icons={tableIcons}
        columns={[
          { title: "Valor", field: "nameValue", editable: "never" },
          { title: "Peso", field: "weight" },
        ]}
        data={props.data}
        actions={[
          {
            icon: tableIcons.View,
            tooltip: "ver",
            onClick: (event, rowData) => {
              setCompSelected(true);
              setNameSelected(rowData.nameValue);
              for (let index = 0; index < props.data.length; index++) {
                let selected = props.data[index].itemsList.filter(
                  (x) => x.name_value === rowData.nameValue
                );
                if (selected.length > 0) {
                  setDataIndicat(
                    props.data[index].itemsList.filter(
                      (x) => x.name_value === rowData.nameValue
                    )
                  );
                }
              }
            },
          },
        ]}
        options={{
          actionsColumnIndex: -1,
        }}
        // editable={{
        //   onRowUpdate: (newData, oldData) =>
        //     new Promise((resolve, reject) => {
        //       setTimeout(() => {
        //         const dataUpdate = [...infoValue];
        //         const index = oldData.tableData.id;
        //         console.log(oldData.id)
        //         // const updateInf = {
        //         //   categoryName : newData.categoryName,
        //         //   description : newData.description,
        //         //   weight : parseInt(newData.weight)
        //         // }
        //         dataUpdate[index] = newData;
        //         setInfoValue([...dataUpdate]);
        //         //updateInfoValue(updateInf, newData.id)
        //         resolve();
        //       }, 1000)
        //     }),
        // }}
        localization={{
          body: {
            emptyDataSourceMessage: "No hay registros para mostrar",
            addTooltip: "Ajouter",
            deleteTooltip: "Supprimer",
            editTooltip: "Editar",
            filterRow: {
              filterTooltip: "Filtrer",
            },
            editRow: {
              deleteText: "Voulez-vous supprimer cette ligne?",
              cancelTooltip: "Cancelar",
              saveTooltip: "Aceptar",
            },
          },
          grouping: {
            placeholder: "Tirer l'entête ...",
            groupedBy: "Grouper par:",
          },
          header: {
            actions: "Acciones",
          },
          pagination: {
            labelDisplayedRows: "{from}-{to} de {count}",
            labelRowsSelect: "lineas",
            labelRowsPerPage: "lineas por página:",
            firstAriaLabel: "Primer página",
            firstTooltip: "Primer página",
            previousAriaLabel: "Página anterior",
            previousTooltip: "Página anterior",
            nextAriaLabel: "Página siguiente",
            nextTooltip: "Página siguiente",
            lastAriaLabel: "Última página",
            lastTooltip: "Última página",
          },
          toolbar: {
            addRemoveColumns: "Ajouter ou supprimer des colonnes",
            nRowsSelected: "{0} ligne(s) sélectionée(s)",
            showColumnsTitle: "Voir les colonnes",
            showColumnsAriaLabel: "Voir les colonnes",
            searchTooltip: "Buscar",
            searchPlaceholder: "Buscar",
          },
        }}
      /> */}
      <br />
      {props.isTracing && compSelected ? (
        <div>
          <div style={{ textAlign: "center" }}>
            <h5>Competencia Transversal {nameSelected}</h5>
          </div>
          <div className="d-flex justify-content-center mt-3">
            <div
              style={
                isMobile
                  ? { border: "2px solid blue", width: "100%" }
                  : { border: "2px solid blue", width: "70%" }
              }
            >
              <div className="ml-3 mt-3">
                <span>Competencia {nameSelected}</span>
              </div>
              <div className="row d-flex align-content-center ml-5 mr-1 mt-3">
                <div className="col-md-3 d-flex align-items-center">
                  <span>Fortalezas</span>
                </div>
                <div className="col-md-8 d-flex align-items-center">
                  <input
                    className="form-control"
                    style={{ border: "2px solid #004F9E" }}
                  />
                </div>
              </div>
              <div className="row d-flex align-content-center ml-5 mr-1">
                <div className="col-md-3 d-flex align-items-center mt-3">
                  <span>Oportunidades de mejora</span>
                </div>
                <div className="col-md-8 d-flex align-items-center">
                  <input
                    className="form-control"
                    style={{ border: "2px solid #004F9E" }}
                  />
                </div>
              </div>
              <div className="ml-5 mt-4 mr-1">
                <div>
                  <span>Indicadores</span>
                </div>
                {dataIndicat.map((item, i) => {
                  return (
                    <div className="mt-2" key={i}>
                      <span>
                        {i + 1}. {item.name_item}
                      </span>
                      <div className="row d-flex align-content-center ml-5 mr-1 mt-3">
                        <div className="col-md-6 d-flex align-items-center justify-content-center">
                          <span>Concepto</span>
                        </div>
                        <div className="col-md-5 d-flex align-items-center">
                          <select
                            className="form-control"
                            id="exampleFormControlSelect1"
                            style={{ border: "2px solid #004F9E" }}
                          >
                            <option>Superior</option>
                            <option>Alto</option>
                            <option>Medio</option>
                            <option>Bajo</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
              <div className="mb-4 mt-2 d-flex align-items-center justify-content-center">
                <button
                  onClick={() => props.customFunction()}
                  style={{
                    backgroundColor: "white",
                    border: "2px solid #004F9E",
                    borderRadius: 5,
                  }}
                >
                  Guardar
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : props.isTracing ? (
        <div></div>
      ) : (
        <div>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th colSpan="3" className="text-center">
                  Competencia transversal {nameSelected}
                </th>
              </tr>
            </thead>
            <tbody>
              {dataIndicat.map((item, i) => {
                return (
                  <tr key={i}>
                    <td className="text-center">{item.name_item}</td>
                    <td>
                      <Checkbox
                        className="text-center"
                        checked={check}
                        name="check"
                        onChange={handleChange}
                      />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
          <div className="d-flex justify-content-center align-items-center mb-4 mt-4">
            <div className="align-self-center">Peso (%)</div>
            <input
              style={{ width: "30%", borderColor: "#004F9E", marginLeft: 15 }}
            />
          </div>
        </div>
      )}
    </div>
  );
}
