import React, { useState } from "react";
import "./styleLogin.css";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import swal from "sweetalert2";
import { login } from "../../services/login";

export default function Login(props) {
  const initialState = {
    email: "",
    password: "",
  };

  const [values, setValues] = useState(initialState);

  const handleOnChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (values.email !== "" && values.password !== "") {
      const rps = await login(values.email, values.password);
      if (rps === true) {
        props.history.push("/profile");
      }
    } else {
      swal("Error", "Usuario y o contraseña incorrecto", "error");
    }
  };

  return (
    <div className="backgroundImgLogin">
      <div className="containerLogin">
        <div className="credenciales">
          <img
            src={require("../../assets/icons/titleLogin.png")}
            className="titleLogin"
            alt="imglogin"
          ></img>
          <div className="formularioLogin">
            <Form onSubmit={handleSubmit}>
              <Form.Group controlId="formEmail" className="formGroup">
                <img
                  src={require("../../assets/icons/iconoUsuario.png")}
                  className="icon"
                  alt="icon"
                ></img>
                <Form.Control
                  style={{ paddingLeft: 40 }}
                  type="email"
                  onChange={handleOnChange}
                  name="email"
                />
                <Form.Label className="lbUsuario">Usuario</Form.Label>
              </Form.Group>
              <Form.Group controlId="formPassword" className="formGroup">
                <img
                  src={require("../../assets/icons/iconoContraseña.png")}
                  className="icon"
                  alt="iconocontra"
                ></img>
                <Form.Control
                  style={{ paddingLeft: 40 }}
                  type="password"
                  onChange={handleOnChange}
                  name="password"
                />
                <Form.Label className="lbUsuario">Contraseña</Form.Label>
              </Form.Group>
              <Form.Text className="forgetPassword">
                <Link to="/forget" className="txtForget">
                  Olvidaste tu contraseña?
                </Link>
              </Form.Text>
              <Button className="btnIngresar" type="submit">
                Ingresar
              </Button>
            </Form>
          </div>
        </div>
        <div className="logo2">
          <img
            src={require("../../assets/icons/logos.png")}
            className="logos"
            alt="logo"
          />
        </div>
      </div>
    </div>
  );
}
