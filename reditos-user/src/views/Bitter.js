import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { consultData } from "../services/consult";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { search } from "../services/consult";

import "./styles.css";
import {
  setBitterList,
  createBoard,
  createBoardUnlimited,
} from "../redux/actions/boardActions";

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  border: "2px solid #EF8400",
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  boxShadow: "0px 0px 5px 5px #EF8400",
  marginBottom: 20,

  textAlign: "center",
  color: "#EF8400",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getItemStyle2 = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  /* border: "2px solid black", */
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  textAlign: "center",
  color: "#EF8400",
  /* width: '50%', */
  /* display: 'flex',
  justifyContent: 'center',
  flexDirection: 'row', */

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "transparent",
  /* padding: grid, */
  width: 250,
  /* border: "2px solid black", */
});

const getListStyle2 = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "#EF8400",
  padding: grid,
  width: "89%",
  justifyContent: "center",
  borderRadius: 20,
  boxShadow: "0px 0px 5px 5px #EF8400",
});

export default function Bitter() {
  const dispatch = useDispatch();

  const { sweetList } = useSelector((state) => state.boardReducer);
  const { bitterList } = useSelector((state) => state.boardReducer);
  const { period } = useSelector((state) => state.boardReducer);

  //typeBoard
  const { typeBoard } = useSelector((state) => state.boardReducer);

  const [state, setstate] = useState([]);
  const [selected, setSelected] = useState([]);

  useEffect(() => {
    consultData("bitter").then((res) => {
      setstate(res);
    });
  }, []);

  const getList = (id) => {
    if (id === "droppable") {
      return state;
    } else {
      return selected;
    }
  };

  const onDragEnd = (result) => {
    const { source, destination } = result;

    // dropped outside the list
    if (!destination) {
      return;
    }
    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        getList(source.droppableId),
        source.index,
        destination.index
      );

      let states = { items };

      if (source.droppableId === "droppable2") {
        states = { selected: items };
        setSelected(states.selected);
      } else {
        setstate(states.items);
      }
    } else {
      const result = move(
        getList(source.droppableId),
        getList(destination.droppableId),
        source,
        destination
      );
      dispatch(setBitterList(result.droppable2));

      setstate(result.droppable);
      setSelected(result.droppable2);
    }
  };

  const searchTarget = async (value) => {
    try {
      await search("bitter", value).then((resp) => {
        setstate(resp);
      });
    } catch (error) {
      throw Error(error);
    }
  };

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity

  const sendDataToBoard = async () => {
    console.log("typeBoard => ", typeBoard);
    const data = JSON.parse(atob(sessionStorage.getItem("rdt")));
    console.log("data => ", data);
    const obj = {
      sweetData: sweetList,
      bitterData: bitterList,
    };
    let year = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    console.log("year => ", year);

    console.log("period => ", period);

    if (typeBoard === "REQUIRED") {
      let objBoard = {
        data: JSON.stringify(obj),
        period: period,
        year: `${year}`,
        checkedByLeader: false,
        idCollaborator: data.collaboratorData.url,
      };
      console.log(objBoard);
      await dispatch(createBoard(objBoard));
    } else if (typeBoard === "UNLIMITED") {
      //Create Unlimited Boards
      let objBoard = {
        data: JSON.stringify(obj),
        month: `${month}`,
        year: `${year}`,
        checkedByLeader: false,
        idCollaborator: data.collaboratorData.url,
      };
      console.log(objBoard);
      await dispatch(createBoardUnlimited(objBoard));
    }
  };

  return (
    <>
      <div className="container-fluid">
        <h1
          style={{
            textAlign: "center",
            fontSize: 58,
            color: "#EF8400",
            fontWeight: "bold",
          }}
          className="resume"
        >
          Exploremos Lo Amargo
        </h1>
        <br />
        <DragDropContext onDragEnd={onDragEnd}>
          <div className="d-flex flex-row">
            <div>
              <input
                onChange={(e) => searchTarget(e.target.value)}
                className="form-control mb-3"
                style={{
                  width: 250,
                  borderColor: "#EF8400",
                  borderWidth: 2,
                  borderRadius: 20,
                }}
                placeholder="Buscar"
              />
              <Droppable droppableId="droppable">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                  >
                    {state.map((item, index) => (
                      <Draggable
                        key={item.id}
                        draggableId={item.id.toString()}
                        index={index}
                      >
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={getItemStyle(
                              snapshot.isDragging,
                              provided.draggableProps.style
                            )}
                          >
                            <h5 className="title">{item.name}</h5>
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </div>
            <Droppable droppableId="droppable2">
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  style={getListStyle2(snapshot.isDraggingOver)}
                  className="ml-5"
                >
                  {selected.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id.toString()}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          className=""
                          style={getItemStyle2(
                            snapshot.isDragging,
                            provided.draggableProps.style
                          )}
                        >
                          <h4 className="title">{item.name}</h4>
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
        </DragDropContext>
      </div>

      <div className="text-center mt-3">
        {/* {atob(sessionStorage.getItem("ld")) === "true" ? (
          <Link to="/conversation/weRecognize">
            <button
              style={{
                backgroundColor: "white",
                border: "2px solid rgb(0, 79, 158)",
              }}
            >
              Guardar
            </button>
          </Link>
        ) : (
          
        )} */}
        <div>
          <Link to="/conversation">
            <button
              onClick={() => {
                Swal("Bien hecho!", "Guardado Exitosamente!", "success");
                window.scroll(0, 0);
                console.log("state bitter sweet => ", bitterList);
                sendDataToBoard();
              }}
              className="btn btn-warning"
            >
              <strong>Guardar</strong>
            </button>
          </Link>
        </div>
      </div>
    </>
  );
}
