import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import edit from "../assets/icons/edit.png";
import swal from "sweetalert2";
import prof from "../assets/icons/profSmall.png";
import contact from "../assets/icons/contact.png";
import call from "../assets/icons/call.png";
import email from "../assets/icons/email.png";
import roles from "../assets/icons/role.png";
import { isMobile } from "react-device-detect";
import ImageUploading from "react-images-uploading";
import "./styles.css";
import { updateInfo } from "../services/update";
import { uploadSingleImage } from "../services/uploadFile";

export default function Profile(props) {
  const [name, setName] = useState("");
  const [id, setId] = useState("");
  const [identification, setIdentification] = useState("");
  const [numContact, setNumContact] = useState("");
  const [mail, setMail] = useState("");
  const [role, setRole] = useState("");
  const [isSubmited, setIsSubmited] = useState(true);
  const [images, setImages] = React.useState([]);
  const [show, setShow] = useState(false);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const maxNumber = 69;
  const handleClose = () => setShow(false);

  useEffect(() => {
    const data = JSON.parse(atob(sessionStorage.getItem("rdt")));
    console.log("data", data);
    if (data.changePass !== true) {
      setShow(true);
    }
    if (sessionStorage.getItem("rdt") !== null) {
      if (data.rol === 1) {
        sessionStorage.setItem(
          "uld",
          btoa(
            JSON.stringify(
              `http://reditos.multimedialab.dev/Project/leaders/${data.leaderData.id}/`
            )
          )
        );
        sessionStorage.setItem("cl", false);
        setName(data.leaderData.leaderName);
        setId(data.leaderData.id);
        setIdentification(data.leaderData.identification);
        setNumContact(data.leaderData.identification);
        setMail(data.email);
        setRole("lider");
        if (data.leaderData.profileImage !== null) {
          let arr = [
            {
              data_url: data.leaderData.profileImage,
            },
          ];
          setImages(arr);
        }
      } else {
        sessionStorage.setItem("cl", true);
        setName(data.collaboratorData.completeName);
        setId(data.collaboratorData.id);
        setIdentification(data.collaboratorData.identification);
        setNumContact(data.collaboratorData.identification);
        setMail(data.email);
        setRole("colaborador");
        if (data.collaboratorData.profileImage !== null) {
          let arr = [
            {
              data_url: data.collaboratorData.profileImage,
            },
          ];
          setImages(arr);
        }
      }
    }
  }, []);

  const onChange = (imageList, addUpdateIndex) => {
    const dataLogged = JSON.parse(atob(sessionStorage.getItem("rdt")));
    // data for submit
    let ProfileImag = imageList[0].file;
    // if (sessionStorage.getItem('profileImage') !== null) {
    //   if (sessionStorage.getItem('profileImage').length > 0) {
    //     ProfileImag = imageList[0].file;
    //   }
    // }
    // else {
    //   ProfileImag = imageList[0].file;
    // }

    uploadSingleImage(ProfileImag).then((uploadFile) => {
      if (atob(sessionStorage.getItem("ld")) === "true") {
        const dataUpdate = {
          profileImage: uploadFile.file,
          identification: id,
          LeaderName: name,
          id_company: dataLogged.leaderData.id_company,
        };
        if (imageList.length > 0) {
          updateInfo("leaders", dataUpdate, id);
          setImages(imageList);
          // sessionStorage.setItem('profileImage', imageList)
        }
      } else {
        const dataUpdate = {
          profileImage: uploadFile.file,
          identification: id,
          completeName: name,
          id_company: dataLogged.collaboratorData.id_company,
          id_leader: dataLogged.collaboratorData.id_leader,
        };
        if (imageList.length > 0) {
          updateInfo("collaborators", dataUpdate, id);
          setImages(imageList);
          // sessionStorage.setItem('profileImage', imageList)
        }
      }
    });
  };

  const handleSubmit = (event) => {
    sessionStorage.setItem("user", name);
    sessionStorage.setItem("idUser", id);
    sessionStorage.setItem("numContact", numContact);
    sessionStorage.setItem("mail", mail);
    sessionStorage.setItem("role", role);
    //alert('A name was submitted: ' + name +id +numContact+ mail+ role);
    if (
      name !== "" &&
      identification !== "" &&
      numContact !== "" &&
      mail !== "" &&
      role !== ""
    ) {
      swal("Información!", "Datos actualizados correctamente!", "success");
      setIsSubmited(true);
    } else {
      swal("Error!", "Los campos no pueden estar vacios", "error");
    }
    event.preventDefault();
  };

  const isEdit = (event) => {
    if (isSubmited === true) {
      setIsSubmited(false);
    }
    event.preventDefault();
  };

  const updatePassword = () => {
    console.log("password,confirmPassword", password, confirmPassword);
    const data = JSON.parse(atob(sessionStorage.getItem("rdt")));
    if (sessionStorage.getItem("rdt") !== null) {
      if (password !== "" && confirmPassword !== "") {
        if (password.length > 5) {
          if (password === confirmPassword) {
            let obj = {
              changePass: true,
              password: password,
            };
            updateInfo("accessLogin", obj, data.id);
            setShow(false);
            swal(
              "Información",
              "La contraseña ha sido actualizada correctamente",
              "success"
            );
          } else {
            swal("Error", "Las contraseñas no coinciden", "warning");
          }
        } else {
          swal(
            "Error",
            "La contraseña debe tener almenos 6 caracteres",
            "warning"
          );
        }
      } else {
        swal("Error!", "Los campos no deben estar vacios", "warning");
      }
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-5 align-self-center text-center mb-4">
          <ImageUploading
            multiple
            value={images}
            onChange={onChange}
            maxNumber={maxNumber}
            dataURLKey="data_url"
          >
            {({
              imageList,
              onImageUpload,
              onImageUpdate,
              onImageRemove,
              isDragging,
              dragProps,
            }) => (
              // write your building UI
              <div className="upload__image-wrapper">
                {images.length > 0 ? (
                  imageList.map((image, index) => (
                    <div
                      key={index}
                      className="image-item"
                      style={{
                        width: 248,
                        margin: "0px auto",
                      }}
                    >
                      <img
                        src={image["data_url"]}
                        alt=""
                        width="70%"
                        style={{ borderRadius: 130 }}
                      />
                      <div>
                        <button
                          className="btn btn-primary mt-3"
                          onClick={() => onImageUpdate(index)}
                        >
                          Actualizar
                        </button>
                      </div>
                    </div>
                  ))
                ) : (
                  <button
                    className="buttonIt"
                    style={
                      isDragging
                        ? { background: "rgb(255,0,0", opacity: 0.1 }
                        : undefined
                    }
                    onClick={onImageUpload}
                    {...dragProps}
                  ></button>
                )}
              </div>
            )}
          </ImageUploading>
        </div>
        <div
          className="col-md-4"
          style={!isSubmited && isMobile ? { marginLeft: "14%" } : {}}
        >
          {isSubmited ? (
            <div>
              <div className="containerapp_content_divForm">
                <img src={prof} alt="logoprofile" />
                <p>{name}</p>
              </div>
              <div className="containerapp_content_divForm">
                <img src={contact} alt="logoContact" />
                <p>{identification}</p>
              </div>
              <div className="containerapp_content_divForm">
                <img src={call} alt="logoPhone" />
                <p>{numContact}</p>
              </div>
              <div className="containerapp_content_divForm">
                <img src={email} alt="logoEmail" />
                <p>{mail}</p>
              </div>
              <div className="containerapp_content_divForm">
                <img src={roles} alt="logoRole" />
                <p>{role}</p>
              </div>
            </div>
          ) : (
            <form onSubmit={handleSubmit}>
              <input
                type="text"
                disabled
                value={name}
                onChange={(event) => setName(event.target.value)}
              />
              <p style={{ color: "#004F9E", borderColor: "#004F9E" }}>Nombre</p>
              <input
                type="number"
                value={identification}
                disabled
                onChange={(event) => setId(event.target.value)}
              />
              <p style={{ color: "#004F9E" }}>Identificación</p>
              <input
                type="number"
                value={numContact}
                onChange={(event) => setNumContact(event.target.value)}
              />
              <p style={{ color: "#004F9E" }}>Número de contacto</p>
              <input
                type="email"
                value={mail !== null ? mail : ""}
                disabled
                onChange={(event) => setMail(event.target.value)}
              />
              <p style={{ color: "#004F9E" }}>Correo electrónico</p>
              <input
                type="text"
                value={role}
                disabled
                onChange={(event) => setRole(event.target.value)}
              />
              <p style={{ color: "#004F9E" }}>Cargo</p>
              <div style={{ textAlign: "center" }}>
                <button
                  type="submit"
                  style={{
                    backgroundColor: "white",
                    border: "2px solid #004F9E",
                  }}
                >
                  Guardar
                </button>
              </div>
            </form>
          )}
        </div>
        <div className="col-md-3">
          <div className={isMobile ? "text-center mt-5" : "text-center"}>
            <button
              style={{ background: "none", border: "none" }}
              onClick={isEdit}
            >
              <img style={{ marginLeft: 18 }} src={edit} alt="leader" />
              <p>Editar</p>
            </button>
          </div>
        </div>
      </div>
      <Modal backdrop="static" centered show={show} onHide={handleClose}>
        <Modal.Body>
          <div>
            <h1 className="text-center mb-4 resume">
              Por seguridad se requiere el cambio de contraseña
            </h1>
            <div className="form-group">
              <h5>Nueva contraseña</h5>
              <input
                onChange={(e) => setPassword(e.target.value)}
                name="formationPlan"
                type="password"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <h5>Confirmar Nueva Contraseña</h5>
              <input
                onChange={(e) => setConfirmPassword(e.target.value)}
                name="formationPlan"
                type="password"
                className="form-control"
              />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            onClick={() => updatePassword()}
            className="btn btn-primary btn-block"
          >
            <h5>
              <strong>Guardar</strong>
            </h5>
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
