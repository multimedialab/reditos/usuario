import React, { useEffect, useState } from "react";
import {useDispatch, useSelector} from 'react-redux';
import { consultData } from "../services/consult";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Link } from "react-router-dom";

import { search } from '../services/consult';

import './styles.css';
import { setSweetList } from "../redux/actions/boardActions";

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  border: "2px solid #00969A",
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  boxShadow: '0px 0px 5px 5px #00969A',
  marginBottom: 20,
  fontFamily: 'Poppins',
  textAlign: 'center',
  color: '#00969A',


  // styles we need to apply on draggables
  ...draggableStyle,
});

const getItemStyle2 = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  /* border: "2px solid black", */
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  textAlign: 'center',
  color: '#00969A',
  

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "transparent",
  /* padding: grid, */
  width: 250,
  /* border:'2px solid black' */
});

const getListStyle2 = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "#00969A",
  padding: grid,
  width: "89%",
  justifyContent: "center",
  borderRadius: 20,
  boxShadow: '0px 0px 5px 5px #00969A',
});

export default function Sweet() {

  const {sweetList} = useSelector(state => state.boardReducer);

  const dispatch = useDispatch();
  const [state, setstate] = useState([]);
  const [selected, setSelected] = useState([]);

  useEffect(() => {
    consultData("sweet").then((res) => {
      setstate(res);
    });
  }, []);


  const getList = (id) => {
    if (id === "droppable") {
      return state;
    } else {
      return selected;
    }
  };

  const onDragEnd = (result) => {
    const { source, destination } = result;

    // dropped outside the list
    if (!destination) {
      return;
    }
    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        getList(source.droppableId),
        source.index,
        destination.index
      );

      let states = { items };

      if (source.droppableId === "droppable2") {
        states = { selected: items };
        setSelected(states.selected);
      } else {
        setstate(states.items);
      }
    } else {      
      const result = move(
        getList(source.droppableId),
        getList(destination.droppableId),
        source,
        destination
      );
      //resultados
      console.log(result.droppable2);
      dispatch(setSweetList(result.droppable2));

      setstate(result.droppable);
      setSelected(result.droppable2);
    }
  };

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity

  const searchTarget = async value => {
    try {
      await search("sweet", value).then((resp) => {
        setstate(resp);
      });

    } catch (error) {
      throw Error(error);
    }
  };

  return (
    <>
      <div className="container-fluid" >
        <h1 style={{ textAlign: "center", fontSize: 58, color: '#00969A', fontWeight: 'bold' }} className="resume">
          Lo Dulce
        </h1>
        <br/>

        <DragDropContext onDragEnd={onDragEnd}>
          <div className="d-flex flex-row">
            <div>
              <input
                onChange={(e) => searchTarget(e.target.value)}
                className="form-control mb-3"
                style={{ width: 250, borderColor: '#00969A', borderWidth: 2, borderRadius: 20 }}
                placeholder="Buscar"
              />
              <Droppable droppableId="droppable">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                  >
                    {state.map((item, index) => (
                      <Draggable
                        key={item.id}
                        draggableId={item.id.toString()}
                        index={index}
                      >
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={getItemStyle(
                              snapshot.isDragging,
                              provided.draggableProps.style
                            )}
                          >
                            <h5 className="title">
                              

                              {item.name}
                              
                            </h5>
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </div>
            <Droppable droppableId="droppable2">
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  style={getListStyle2(snapshot.isDraggingOver)}
                  className="ml-5"
                >
                  {selected.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id.toString()}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          className=""
                          style={getItemStyle2(
                            snapshot.isDragging,
                            provided.draggableProps.style
                          )}
                        >
                          <h4 className="title">
                            {item.name}
                          </h4>

                          
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
        </DragDropContext>
      </div>
      <div className="text-center mt-3">
        <Link to="/conversation/bitter">
          <button onClick={() => {window.scroll(0,0); console.log('state sweetList => ', sweetList)}} className="btn btn-info">
            <strong>Guardar</strong>
          </button>
        </Link>
      </div>
    </>
  );
}
