import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { consultData } from "../services/consult";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Link } from "react-router-dom";
import swal from "sweetalert2";
import { search } from "../services/consult";

import { Modal } from "react-bootstrap";

import "./styles.css";
import {
  updateBoard,
  updateBoardUnlimited,
  createBitterCard,
  setBitterListLeader,
} from "../redux/actions/boardActions";

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  border: "2px solid #EF8400",
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  boxShadow: "0px 0px 5px 5px #EF8400",
  marginBottom: 20,

  textAlign: "center",
  color: "#EF8400",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getItemStyle2 = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  /* border: "2px solid black", */
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  textAlign: "center",
  color: "#EF8400",
  /* width: '50%', */
  /* display: 'flex',
  justifyContent: 'center',
  flexDirection: 'row', */

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "transparent",
  /* padding: grid, */
  width: 250,
  /* border: "2px solid black", */
});

const getListStyle2 = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "#EF8400",
  padding: grid,
  width: "89%",
  justifyContent: "center",
  borderRadius: 20,
  boxShadow: "0px 0px 5px 5px #EF8400",
});

export default function BitterLeader() {
  const dispatch = useDispatch();

  const { sweetListLeader } = useSelector((state) => state.boardReducer);
  const { bitterListLeader } = useSelector((state) => state.boardReducer);
  const { period } = useSelector((state) => state.boardReducer);
  const { boardData } = useSelector((state) => state.boardReducer);

  const [state, setstate] = useState([]);
  const [selected, setSelected] = useState([]);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);

  const [card, setCard] = useState({ name: "" });

  useEffect(() => {
    consultData("bitter").then((res) => {
      setstate(res);
    });
  }, []);

  const handleChange = (e) => {
    setCard({ name: e });
    console.log("name card => ", e);
  };

  const onCreateNewCard = async () => {
    await dispatch(createBitterCard(card));
    handleClose();
    swal("Agregada!", "Se ha guardado la tarjeta", "success");
  };

  const getList = (id) => {
    if (id === "droppable") {
      return state;
    } else {
      return selected;
    }
  };

  const onDragEnd = (result) => {
    const { source, destination } = result;

    // dropped outside the list
    if (!destination) {
      return;
    }
    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        getList(source.droppableId),
        source.index,
        destination.index
      );

      let states = { items };

      if (source.droppableId === "droppable2") {
        states = { selected: items };
        setSelected(states.selected);
      } else {
        setstate(states.items);
      }
    } else {
      const result = move(
        getList(source.droppableId),
        getList(destination.droppableId),
        source,
        destination
      );
      dispatch(setBitterListLeader(result.droppable2));

      setstate(result.droppable);
      setSelected(result.droppable2);
    }
  };

  const searchTarget = async (value) => {
    try {
      await search("bitter", value).then((resp) => {
        setstate(resp);
      });
    } catch (error) {
      throw Error(error);
    }
  };

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity

  const sendDataToBoard = async () => {
    //const data = JSON.parse(atob(sessionStorage.getItem('rdt')))
    console.log("data => ", boardData);

    const obj = {
      sweetDataLeader: sweetListLeader,
      bitterDataLeader: bitterListLeader,
    };
    let year = new Date().getFullYear();
    console.log("year => ", year);

    let objBoard = {
      leaderAdditions: JSON.stringify(obj),
      period: period,
      year: `${year}`,
      checkedByLeader: false,
      idCollaborator: boardData.idCollaborator,
    };
    console.log(objBoard);

    if (sessionStorage.getItem("typeBoard") === "REQUIRED") {
      await dispatch(updateBoard(boardData.id, objBoard));
    } else if (sessionStorage.getItem("typeBoard") === "UNLIMITED") {
      await dispatch(updateBoardUnlimited(boardData.id, objBoard));
    }
  };

  return (
    <>
      <div className="container-fluid">
        <div
          style={{ display: "flex", justifyContent: "center", width: "100%" }}
        >
          <h1
            style={{
              textAlign: "center",
              fontSize: 58,
              color: "#EF8400",
              fontWeight: "bold",
            }}
            className="resume"
          >
            Exploremos Lo Amargo
          </h1>
        </div>
        {/* <div
          style={{ display: "flex", width: "100%", justifyContent: "flex-end" }}
        >
          <button
            onClick={() => handleShow()}
            className="btn btn-outline-primary"
          >
            Agregar Tarjeta
          </button>
        </div> */}
        <br />
        <DragDropContext onDragEnd={onDragEnd}>
          <div className="d-flex flex-row">
            <div>
              <input
                onChange={(e) => searchTarget(e.target.value)}
                className="form-control mb-3"
                style={{
                  width: 250,
                  borderColor: "#EF8400",
                  borderWidth: 2,
                  borderRadius: 20,
                }}
                placeholder="Buscar"
              />
              <Droppable droppableId="droppable">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                  >
                    {state.map((item, index) => (
                      <Draggable
                        key={item.id}
                        draggableId={item.id.toString()}
                        index={index}
                      >
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={getItemStyle(
                              snapshot.isDragging,
                              provided.draggableProps.style
                            )}
                          >
                            <h5 className="title">{item.name}</h5>
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </div>
            <Droppable droppableId="droppable2">
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  style={getListStyle2(snapshot.isDraggingOver)}
                  className="ml-5"
                >
                  {selected.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id.toString()}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          className=""
                          style={getItemStyle2(
                            snapshot.isDragging,
                            provided.draggableProps.style
                          )}
                        >
                          <h4 className="title">{item.name}</h4>
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
        </DragDropContext>
      </div>

      <div className="text-center mt-3">
        {/* {atob(sessionStorage.getItem("ld")) === "true" ? (
          <Link to="/conversation/weRecognize">
            <button
              style={{
                backgroundColor: "white",
                border: "2px solid rgb(0, 79, 158)",
              }}
            >
              Guardar
            </button>
          </Link>
        ) : (
          
        )} */}
        <div>
          <Link to="/conversation/additions">
            <button
              onClick={() => {
                swal("Bien hecho!", "Guardado Exitosamente!", "success");
                window.scroll(0, 0);
                console.log("data BitterListLeader => ", bitterListLeader);
                sendDataToBoard();
              }}
              className="btn btn-warning"
            >
              <strong>Guardar</strong>
            </button>
          </Link>
        </div>
      </div>
      <Modal backdrop="static" centered show={show} onHide={handleClose}>
        <Modal.Body>
          <div>
            <div className="form-group">
              <h5>Titulo de Tarjeta</h5>
              <input
                onChange={(e) => handleChange(e.target.value)}
                name="name"
                type="text"
                className="form-control"
              />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              width: "100%",
              flexWrap: "wrap",
            }}
          >
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                width: "100%",
              }}
            >
              <small>
                <p>
                  Tu perfil cuenta con permisos de agregar nuevas tarjetas a
                  esta lista
                </p>
              </small>
            </div>

            <div
              style={{
                display: "flex",
                justifyContent: "center",
                width: "100%",
              }}
            >
              <small>
                <p className="text-danger">
                  Nota: En caso de cometer un error, reportalo al Administrador
                </p>
              </small>
            </div>
          </div>
          <button
            onClick={() => onCreateNewCard()}
            className="btn btn-block"
            style={{ backgroundColor: "#EF8400", color: "#fff" }}
          >
            <h5>
              <strong>Agregar</strong>
            </h5>
          </button>
          <button
            onClick={() => handleClose()}
            className="btn btn-secondary btn-block"
          >
            <h5>
              <strong>Cerrar</strong>
            </h5>
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
