import React, { useEffect, useState } from "react";
import { consultData } from "../services/consult";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import "./styles.css";

import Swal from "sweetalert2";

import { AiFillCheckSquare } from "react-icons/ai";

import { search } from "../services/consult";
import {
  updateBoard,
  setWeRecognizeList,
  updateBoardUnlimited,
} from "../redux/actions/boardActions";
import { useSelector, useDispatch } from "react-redux";

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer

  flexDirection: "column",
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  border: "2px solid #E2003A",
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  boxShadow: "0px 0px 5px 5px #E2003A",
  marginBottom: 20,
  textAlign: "center",
  // styles we need to apply on draggables
  ...draggableStyle,
});

const getItemStyle2 = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer

  flexDirection: "column",
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  /* border: "2px solid black", */
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  textAlign: "center",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "transparent",
  /* padding: grid, */
  width: 250,
  /* border: "2px solid black", */
});

const getListStyle2 = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "#E2003A",
  padding: grid,
  width: "89%",
  justifyContent: "center",
  borderRadius: 20,
  boxShadow: "0px 0px 5px 5px #E2003A",
});

export default function WeRecognize(props) {
  const [state, setstate] = useState([]);
  const [selected, setSelected] = useState([]);
  const dispatch = useDispatch();
  const { sweetList } = useSelector((state) => state.boardReducer);
  const { bitterList } = useSelector((state) => state.boardReducer);
  const { additionList } = useSelector((state) => state.boardReducer);
  const { boardData } = useSelector((state) => state.boardReducer);

  //typeBoard
  const { typeBoard } = useSelector((state) => state.boardReducer);

  useEffect(() => {
    consultData("weRecognize").then((res) => {
      setstate(res);
    });
  }, []);

  const getList = (id) => {
    if (id === "droppable") {
      return state;
    } else {
      return selected;
    }
  };

  const onDragEnd = (result) => {
    const { source, destination } = result;

    // dropped outside the list
    if (!destination) {
      return;
    }
    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        getList(source.droppableId),
        source.index,
        destination.index
      );

      let states = { items };

      if (source.droppableId === "droppable2") {
        states = { selected: items };
        setSelected(states.selected);
      } else {
        setstate(states.items);
      }
    } else {
      const result = move(
        getList(source.droppableId),
        getList(destination.droppableId),
        source,
        destination
      );
      setstate(result.droppable);
      setSelected(result.droppable2);
    }
  };

  const searchTarget = async (value) => {
    try {
      await search("weRecognize", value).then((resp) => {
        setstate(resp);
      });
    } catch (error) {
      throw Error(error);
    }
  };

  const finishSummary = () => {
    console.log("typeBoard => ", typeBoard);
    Swal({
      title: "Estas seguro?",
      text: "Una vez, guardada no seras capaz de cambiar esta conversación!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        let obj = {
          sweetData: sweetList,
          bitterData: bitterList,
          additionsData: additionList,
          recognize: selected,
        };
        dispatch(setWeRecognizeList(selected));
        if (sessionStorage.getItem("typeBoard") === "REQUIRED") {
          dispatch(
            updateBoard(boardData.id, {
              data: JSON.stringify(obj),
              checkedByLeader: "True",
              idCollaborator: boardData.idCollaborator,
            })
          );
          Swal("Correcto! Tu conversación ha sido guardada!", {
            icon: "success",
          });
          props.history.push("/conversation/summary");
          window.scroll(0, 0);
        } else if (sessionStorage.getItem("typeBoard") === "UNLIMITED") {
          dispatch(
            updateBoardUnlimited(boardData.id, {
              data: JSON.stringify(obj),
              checkedByLeader: "True",
              idCollaborator: boardData.idCollaborator,
            })
          );

          Swal("Correcto! Tu conversación ha sido guardada!", {
            icon: "success",
          });
          props.history.push("/conversation/summary");
          window.scroll(0, 0);
        }
      } else {
        Swal("Tu conversacion no fue guardada");
      }
    });
  };

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity

  return (
    <>
      <div className="ml-4 mr-4">
        <h1
          style={{
            textAlign: "center",
            fontSize: 58,
            color: "#E2003A",
            fontWeight: "bold",
          }}
          className="resume"
        >
          Reconocemos
        </h1>
        <br />

        <DragDropContext onDragEnd={onDragEnd}>
          <div className="d-flex flex-row">
            <div>
              <input
                onChange={(e) => searchTarget(e.target.value)}
                className="form-control mb-3"
                style={{
                  width: 250,
                  borderColor: "#E2003A",
                  borderWidth: 2,
                  borderRadius: 20,
                }}
                placeholder="Buscar"
              />
              <Droppable droppableId="droppable">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                  >
                    {state.map((item, index) => (
                      <Draggable
                        key={item.id}
                        draggableId={item.id.toString()}
                        index={index}
                      >
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={getItemStyle(
                              snapshot.isDragging,
                              provided.draggableProps.style
                            )}
                          >
                            <img
                              style={{ objectFit: "contain" }}
                              src={item.emoji}
                              alt=""
                            />
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                              }}
                            >
                              <h5 className="title"> {item.name} </h5>
                            </div>
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </div>
            <Droppable droppableId="droppable2">
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  style={getListStyle2(snapshot.isDraggingOver)}
                  className="ml-5"
                >
                  {selected.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id.toString()}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          className=""
                          style={getItemStyle2(
                            snapshot.isDragging,
                            provided.draggableProps.style
                          )}
                        >
                          <img
                            style={{ objectFit: "contain" }}
                            src={item.emoji}
                            alt=""
                          />
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "column",
                            }}
                          >
                            <h4 className="title"> {item.name} </h4>
                          </div>
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
        </DragDropContext>
      </div>
      <div className="text-center mt-3">
        <button
          onClick={() => {
            finishSummary();
          }}
          className="btn btn-success"
        >
          <AiFillCheckSquare size={25} className="mr-2" />
          Terminar Revisión
        </button>
      </div>
    </>
  );
}
