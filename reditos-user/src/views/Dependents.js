import React, { useState, useEffect, useCallback } from "react";
import Swal from "sweetalert2";
//import MaterialTable from "material-table";
//import tableIcons from "../constants/MaterialIcons";
import axios from "axios";
import { body, url } from "../constants/auth";
import Objectives from "../views/Categories/Objectives";
import { useDispatch } from "react-redux";
import SelectedUser from "../components/SelectedUser";
import Values from "./Categories/Values";
import { createTracing } from "../redux/actions/tracingActions";
import { getSettings } from "../redux/actions/userActions";

//Utils
import { formatDateString } from "../utils/formatDate";

export default function PersonsGroup(props) {
  const dispatch = useDispatch();
  const [info, setInfo] = useState([]);
  const [name, setName] = useState("o");
  const [idCol, setIdCol] = useState("");
  const [infoCat, setInfoCat] = useState([]);
  const [nameCat, setNameCat] = useState("initial");
  const [currentView, setCurrentView] = useState("initial");
  const [datesTracing, setDateTracing] = useState({});

  //This state is for filter the data between values and competencies
  const [infoValue, setInfoValue] = useState([]);

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      console.log("error", error.message);
    }
  };

  const consult = useCallback(async () => {
    const token = await getToken();
    fetch(`${url.urlBase}/collaborators/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        setInfo(
          json.results.filter(
            (x) =>
              x.id_leader === JSON.parse(atob(sessionStorage.getItem("uld")))
          )
        );
      })
      .catch((error) => console.error(error));
  }, []);

  const getCategory = useCallback(async () => {
    const token = await getToken();
    fetch(`${url.urlBase}/category/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        setInfoCat(json.results);
      })
      .catch((error) => console.error(error));
  }, []);

  const getPeriodTracing = useCallback(() => {
    let today = new Date().getTime();
    dispatch(getSettings()).then((item) => {
      let initDate = new Date(item.initDateTracingOne);
      let endDate = new Date(item.endDateTracingOne);
      if (initDate.getTime() < today && today < endDate.getTime()) {
        setDateTracing({
          initDate: item.initDateTracingOne,
          endDate: item.endDateTracingOne,
          period: 1,
        });
      } else {
        setDateTracing({
          initDate: item.initDateTracingTwo,
          endDate: item.endDateTracingTwo,
          period: 2,
        });
      }
    });
  }, [dispatch]);

  useEffect(() => {
    consult();
    getPeriodTracing();
    getCategory();
  }, [consult, getCategory, getPeriodTracing]);

  const createAndValidTracing = () => {
    let dataToPush = {
      startDate: datesTracing.initDate,
      endDate: datesTracing.endDate,
      blockDate: null,
      blockReason: null,
      year: new Date().getFullYear(),
      period: datesTracing.period,
      collaborator: sessionStorage.getItem("url"),
      leader: JSON.parse(atob(sessionStorage.getItem("uld"))),
    };

    dispatch(createTracing(dataToPush, formatDateString(new Date()))).then(
      (resp) => {
        if (resp.status) {
          sessionStorage.setItem("idt", resp.data.url);
          setCurrentView("category");
        } else {
          Swal.fire({
            title: "Lo sentimos!",
            text:
              "El colaborador ya cuenta con el segumiento para el periodo en curso.",
            icon: "info",
            showCancelButton: true,
            confirmButtonText: "Revisar Segumiento",
          }).then((val) => {
            if (val.isConfirmed) {
              if (datesTracing.period === 1) {
                props.history.push("/tracing");
              } else {
                props.history.push("/tracing1");
              }
            }
          });
        }
      }
    );
  };

  return (
    <>
      <div className="container">
        {currentView === "category" ? (
          <div>
            <SelectedUser name={name} idCol={idCol} />
            <br />
            <br />
            <div className="row">
              <div className="col-12">
                {/*  <MaterialTable
                  title="CATEGORÍAS DE EVALUACIÓN"
                  icons={tableIcons}
                  columns={[
                    {
                      title: "Categorías",
                      field: "categoryName",
                      editable: "never",
                    },
                    { title: "Peso", field: "weight" },
                  ]}
                  data={infoCat}
                  actions={[
                    {
                      icon: tableIcons.View,
                      tooltip: "Ver",
                      onClick: (event, rowData) => {
                        const result = infoCat.filter(
                          (x) => x.categoryName === rowData.categoryName
                        );
                        setInfoValue(result[0].valuesList);
                        if (rowData.categoryName === "OBJETIVOS") {
                          setNameCat(rowData.categoryName);
                          setCurrentView("diff");
                        } else {
                          setNameCat(rowData.categoryName);
                          setCurrentView("subcategory");
                        }
                      },
                    },
                  ]}
                  options={{
                    actionsColumnIndex: -1,
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: "No hay registros para mostrar",
                      addTooltip: "Ajouter",
                      deleteTooltip: "Eliminar",
                      editTooltip: "Editar",
                      filterRow: {
                        filterTooltip: "Filtrer",
                      },
                      editRow: {
                        deleteText:
                          "Esta seguro que desea eliminar el registro?",
                        cancelTooltip: "Cancelar",
                        saveTooltip: "Aceptar",
                      },
                    },
                    grouping: {
                      placeholder: "Tirer l'entête ...",
                      groupedBy: "Grouper par:",
                    },
                    header: {
                      actions: "Acciones",
                    },
                    pagination: {
                      labelDisplayedRows: "{from}-{to} de {count}",
                      labelRowsSelect: "lineas",
                      labelRowsPerPage: "lineas por página:",
                      firstAriaLabel: "Primer página",
                      firstTooltip: "Primer página",
                      previousAriaLabel: "Página anterior",
                      previousTooltip: "Página anterior",
                      nextAriaLabel: "Página siguiente",
                      nextTooltip: "Página siguiente",
                      lastAriaLabel: "Última página",
                      lastTooltip: "Última página",
                    },
                    toolbar: {
                      addRemoveColumns: "Ajouter ou supprimer des colonnes",
                      nRowsSelected: "{0} ligne(s) sélectionée(s)",
                      showColumnsTitle: "Voir les colonnes",
                      showColumnsAriaLabel: "Voir les colonnes",
                      searchTooltip: "Buscar",
                      searchPlaceholder: "Buscar",
                    },
                  }}
                /> */}
              </div>
            </div>
          </div>
        ) : (
          <div></div>
        )}
        {currentView === "initial" ? (
          <div>
            <div className="row">
              <div className="col-12">
                {/* <MaterialTable
                  title="Colaboradores a cargo"
                  icons={tableIcons}
                  columns={[
                    {
                      title: "Identificador del colaborador",
                      field: "identification",
                    },
                    { title: "Nombre del colaborador", field: "completeName" },
                  ]}
                  data={info}
                  actions={[
                    {
                      icon: tableIcons.Assign,
                      tooltip: "Asignar",
                      onClick: (event, rowData) => {
                        setIdCol(rowData.identification);
                        setName(rowData.completeName);
                        sessionStorage.setItem("nameCol", rowData.completeName);
                        sessionStorage.setItem(
                          "idColab",
                          rowData.identification
                        );
                        sessionStorage.setItem("url", rowData.url);
                        sessionStorage.setItem("idCompany", rowData.id_company);
                        sessionStorage.setItem("id_leader", rowData.id_leader);
                        sessionStorage.setItem("id", rowData.id);
                        sessionStorage.setItem(
                          "profileImage",
                          rowData.profileImage
                        );
                        createAndValidTracing();
                      },
                    },
                  ]}
                  options={{
                    actionsColumnIndex: -1,
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: "No hay registros para mostrar",
                      addTooltip: "Ajouter",
                      deleteTooltip: "Supprimer",
                      editTooltip: "Editar",
                      filterRow: {
                        filterTooltip: "Filtrer",
                      },
                      editRow: {
                        deleteText: "Voulez-vous supprimer cette ligne?",
                        cancelTooltip: "Cancelar",
                        saveTooltip: "Aceptar",
                      },
                    },
                    grouping: {
                      placeholder: "Tirer l'entête ...",
                      groupedBy: "Grouper par:",
                    },
                    header: {
                      actions: "Acciones",
                    },
                    pagination: {
                      labelDisplayedRows: "{from}-{to} de {count}",
                      labelRowsSelect: "lineas",
                      labelRowsPerPage: "lineas por página:",
                      firstAriaLabel: "Primer página",
                      firstTooltip: "Primer página",
                      previousAriaLabel: "Página anterior",
                      previousTooltip: "Página anterior",
                      nextAriaLabel: "Página siguiente",
                      nextTooltip: "Página siguiente",
                      lastAriaLabel: "Última página",
                      lastTooltip: "Última página",
                    },
                    toolbar: {
                      addRemoveColumns: "Ajouter ou supprimer des colonnes",
                      nRowsSelected: "{0} ligne(s) sélectionée(s)",
                      showColumnsTitle: "Voir les colonnes",
                      showColumnsAriaLabel: "Voir les colonnes",
                      searchTooltip: "Buscar",
                      searchPlaceholder: "Buscar",
                    },
                  }}
                /> */}
              </div>
            </div>
          </div>
        ) : (
          <div></div>
        )}

        {currentView === "subcategory" ? (
          <div>
            <SelectedUser name={name} idCol={idCol} />
            <br />
            <br />
            <Values
              name={nameCat}
              data={infoValue}
              customFunctionBack={() => setCurrentView("category")}
              customFunction={() => {
                setCurrentView("initial");
                Swal("Bien hecho!", "Registro guardado!", "success");
              }}
            />
          </div>
        ) : (
          <div></div>
        )}
        {nameCat === "OBJETIVOS" ? (
          <div>
            <SelectedUser name={name} idCol={idCol} />
            <br />
            <Objectives />

            <div className="text-center">
              <button
                onClick={() => {
                  setCurrentView("category");
                  setNameCat("diff");
                }}
                style={{
                  backgroundColor: "white",
                  border: "2px solid #004F9E",
                  borderRadius: 5,
                }}
              >
                Atras
              </button>
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </>
  );
}
