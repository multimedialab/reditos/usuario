import React, { useState, useCallback, useEffect } from "react";
import loDulce from "../assets/icons/Coffeee.png";
import LoAmargo from "../assets/icons/CoffeeAmargo.png";
import Reconocemos from "../assets/icons/Reconocemos.png";
import Adiciones from "../assets/icons/Adiciones.png";
import "./styles.css";
import { Modal, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import {
  getBoardCollaborators,
  getBoardColUnlimited,
  searchColaborator,
} from "../redux/actions/boardActions";
import swal from "sweetalert2";
//create excel report
import ReactExport from "react-data-export";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function Summary() {
  const [show, setShow] = useState(false);
  const [title, setTitle] = useState("");
  const [content, setContent] = useState([]);
  const [period, setPeriod] = useState(1);
  const [yearFilter, setYearFilter] = useState(0);
  const [nameColab, setNameColab] = useState("");
  const [individualResume, setIndividualResume] = useState([]);
  const [dataColaborators, setDataColaborators] = useState([]);
  const [sweetList, setSweetList] = useState([]);
  const [bitterList, setBitterList] = useState([]);
  const [weRecognizeList, setWeRecognizeList] = useState([]);
  const [additionList, setAdditionList] = useState([]);
  const [boarRequired, setBoarRequired] = useState("required");
  const [years, setYears] = useState([]);
  const [bitterLeader, setBitterLeader] = useState("");
  const [sweetLeader, setSweetLeader] = useState("");
  const [showLeaderAdittions, setShowLeaderAdittions] = useState(false);
  const [generalReport, setGeneralReport] = useState([]);
  const handleClose = () => setShow(false);
  const dispatch = useDispatch();

  const handleShow = (name) => {
    switch (name) {
      case "sweet":
        setTitle("LO DULCE");
        setContent(sweetList);
        break;

      case "bitter":
        setTitle("LO AMARGO");
        setContent(bitterList);
        break;

      case "recognize":
        setTitle("RECONOCEMOS");
        setContent(weRecognizeList);
        break;

      case "additions":
        setContent(additionList);
        setTitle("ADICIONES");
        break;
      default:
        break;
    }
    setShow(true);
  };

  const getRandomColor = () => {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  const getDataCollaborators = useCallback(
    async (period, year) => {
      let idLead = JSON.parse(atob(sessionStorage.getItem("uld")));
      const idLeader = idLead.split("/")[5];
      await dispatch(
        getBoardCollaborators(year, period, idLeader, "True")
      ).then((resp) => {
        if (resp.length !== 0) {
          setDataColaborators(resp);
          console.log("resp ", resp);
          let datLead = JSON.parse(atob(sessionStorage.getItem("rdt")));
          let excelData = [];
          resp.forEach((element, i) => {
            //setNameColab(element.nameCollaborator)
            let sweet = JSON.parse(element.data).sweetData;
            sweet[i] = {
              nameColab: element.nameCollaborator,
              nameLeader: datLead.leaderData.leaderName,
              ...sweet[0],
            };
            setSweetList(sweet);
            setBitterList(JSON.parse(element.data).bitterData);
            setAdditionList(JSON.parse(element.data).additionsData);
            setWeRecognizeList(JSON.parse(element.data).recognize);
          });

          resp.forEach((col, index) => {
            let board = JSON.parse(col.data);
            if (col.leaderAdditions !== null) {
              let leaderBoard = JSON.parse(col.leaderAdditions);
              let fullboard = { ...board, ...leaderBoard };
              Object.keys(fullboard).forEach((key, i) => {
                fullboard[key].forEach((card, j) => {
                  excelData.push({
                    "Nombre colaborador": col.nameCollaborator,
                    "Nombre lider": datLead.leaderData.leaderName,
                    "Tipo tarjeta":
                      key === "sweetDataLeader"
                        ? "Lo dulce lider"
                        : key === "bitterDataLeader"
                        ? "Lo amargo lider"
                        : key === "sweetData"
                        ? "Lo dulce"
                        : key === "bitterData"
                        ? "Lo amargo"
                        : key === "additionsData"
                        ? "Adiciones"
                        : key === "recognize"
                        ? "Reconocemos"
                        : "",
                    Tarjeta: card.name,
                    Conversacion:
                      card.selectedPlus !== undefined
                        ? card.selectedPlus
                          ? "De acuerdo"
                          : "En desacuerdo"
                        : "",
                    "Plan de accion":
                      card.actionPlan !== undefined ? card.actionPlan : "",
                    Fecha: card.date,
                  });
                });
              });
            } else {
              Object.keys(board).forEach((key, i) => {
                board[key].forEach((card, j) => {
                  excelData.push({
                    "Nombre colaborador": col.nameCollaborator,
                    "Nombre lider": datLead.leaderData.leaderName,
                    "Tipo tarjeta":
                      key === "sweetData"
                        ? "Lo dulce"
                        : key === "bitterData"
                        ? "Lo amargo"
                        : key === "additionsData"
                        ? "Adiciones"
                        : key === "recognize"
                        ? "Reconocemos"
                        : "",
                    Tarjeta: card.name,
                    Conversacion:
                      card.selectedPlus !== undefined
                        ? card.selectedPlus
                          ? "De acuerdo"
                          : "En desacuerdo"
                        : "",
                    "Plan de accion":
                      card.actionPlan !== undefined ? card.actionPlan : "",
                    Fecha: card.date,
                  });
                });
              });
            }
          });

          setGeneralReport(excelData);
          setIndividualResume([]);
          setNameColab("");
        } else {
          swal(
            "oops!",
            "Lo sentimos no se encontraron resultados con los filtros seleccionados"
          );
          setDataColaborators([]);
          setGeneralReport([]);
          setIndividualResume([]);
          setNameColab("");
        }
      });
    },
    [dispatch]
  );

  const getDataSearched = async (nameSearch) => {
    let idLead = JSON.parse(atob(sessionStorage.getItem("uld")));
    const idLeader = idLead.split("/")[5];
    await dispatch(
      searchColaborator(yearFilter, period, idLeader, "True", nameSearch)
    ).then((resp) => {
      if (resp.length !== 0) {
        setDataColaborators(resp);
        setIndividualResume([]);
        setNameColab("");
      } else {
        swal(
          "oops!",
          "Lo sentimos no se encontraron resultados con los filtros seleccionados"
        );
        setDataColaborators([]);
        setIndividualResume([]);
        setNameColab("");
      }
    });
  };

  const calculateYears = () => {
    let currentYear = new Date().getFullYear();
    setYears([currentYear]);
    var prevYear = new Date();
    for (let i = 0; i < 5; i++) {
      prevYear.setFullYear(prevYear.getFullYear() - 1);
      let year = prevYear.getFullYear();
      setYears((old) => [...old, year]);
    }
  };

  const getDataColabUnlimited = useCallback(
    async (year) => {
      let idLead = JSON.parse(atob(sessionStorage.getItem("uld")));
      const idLeader = idLead.split("/")[5];
      await dispatch(getBoardColUnlimited(year, idLeader, "True")).then(
        (resp) => {
          setDataColaborators(resp);
          let datLead = JSON.parse(atob(sessionStorage.getItem("rdt")));
          let excelData = [];
          resp.forEach((element, i) => {
            //setNameColab(element.nameCollaborator)
            let sweet = JSON.parse(element.data).sweetData;
            sweet[i] = {
              nameColab: element.nameCollaborator,
              nameLeader: datLead.leaderData.leaderName,
              ...sweet[0],
            };
            setSweetList(sweet);
            setBitterList(JSON.parse(element.data).bitterData);
            setAdditionList(JSON.parse(element.data).additionsData);
            setWeRecognizeList(JSON.parse(element.data).recognize);
          });

          resp.forEach((col, index) => {
            let board = JSON.parse(col.data);
            Object.keys(board).forEach((key, i) => {
              board[key].forEach((card, j) => {
                excelData.push({
                  "Nombre colaborador": col.nameCollaborator,
                  "Nombre lider": datLead.leaderData.leaderName,
                  "Tipo tarjeta":
                    key === "sweetData"
                      ? "Lo dulce"
                      : key === "bitterData"
                      ? "Lo amargo"
                      : key === "additionsData"
                      ? "Adiciones"
                      : key === "recognize"
                      ? "Reconocemos"
                      : "",
                  Tarjeta: card.name,
                  Conversacion:
                    card.selectedPlus !== undefined
                      ? card.selectedPlus
                        ? "De acuerdo"
                        : "En desacuerdo"
                      : "",
                  "Plan de accion": card.actionPlan,
                  Fecha: card.timestamp,
                });
              });
            });
          });
          setGeneralReport(excelData);
        }
      );
    },
    [dispatch]
  );

  useEffect(() => {
    let dateSystem = new Date();
    let month = dateSystem.getMonth() + 1;
    let period = 0;
    if (month >= 1 && month <= 6) {
      period = 1;
      setPeriod(1);
    } else {
      period = 2;
      setPeriod(2);
    }
    const year = new Date().getFullYear();
    setYearFilter(year);
    getDataCollaborators(period, year);
    calculateYears();
  }, [getDataCollaborators, getDataColabUnlimited]);

  const setInfoBoard = (item) => {
    let datLead = JSON.parse(atob(sessionStorage.getItem("rdt")));
    setNameColab(item.nameCollaborator);
    console.log(`item`, item);
    let sweet = JSON.parse(item.data).sweetData;
    sweet[0] = {
      nameColab: item.nameCollaborator,
      nameLeader: datLead.leaderData.leaderName,
      ...sweet[0],
    };
    setSweetList(sweet);
    setBitterList(JSON.parse(item.data).bitterData);
    setAdditionList(JSON.parse(item.data).additionsData);
    setWeRecognizeList(JSON.parse(item.data).recognize);

    let arrTemp = [];
    let board = JSON.parse(item.data);
    if (item.leaderAdditions !== null) {
      let leaderBoard = JSON.parse(item.leaderAdditions);
      let fullboard = { ...board, ...leaderBoard };
      Object.keys(fullboard).forEach((key, i) => {
        fullboard[key].forEach((card, j) => {
          arrTemp.push({
            "Nombre colaborador": item.nameCollaborator,
            "Nombre lider": datLead.leaderData.leaderName,
            "Tipo tarjeta":
              key === "sweetDataLeader"
                ? "Lo dulce lider"
                : key === "bitterDataLeader"
                ? "Lo amargo lider"
                : key === "sweetData"
                ? "Lo dulce"
                : key === "bitterData"
                ? "Lo amargo"
                : key === "additionsData"
                ? "Adiciones"
                : key === "recognize"
                ? "Reconocemos"
                : "",
            Tarjeta: card.name,
            Conversacion:
              card.selectedPlus !== undefined
                ? card.selectedPlus
                  ? "De acuerdo"
                  : "En desacuerdo"
                : "",
            "Plan de accion":
              card.actionPlan !== undefined ? card.actionPlan : "",
            Fecha: card.date,
          });
        });
      });
    } else {
      Object.keys(board).forEach((key, i) => {
        board[key].forEach((card, j) => {
          arrTemp.push({
            "Nombre colaborador": item.nameCollaborator,
            "Nombre lider": datLead.leaderData.leaderName,
            "Tipo tarjeta":
              key === "sweetData"
                ? "Lo dulce"
                : key === "bitterData"
                ? "Lo amargo"
                : key === "additionsData"
                ? "Adiciones"
                : key === "recognize"
                ? "Reconocemos"
                : "",
            Tarjeta: card.name,
            Conversacion:
              card.selectedPlus !== undefined
                ? card.selectedPlus
                  ? "De acuerdo"
                  : "En desacuerdo"
                : "",
            "Plan de accion":
              card.actionPlan !== undefined ? card.actionPlan : "",
            Fecha: card.date,
          });
        });
      });
    }
    console.log(`arrTemp`, arrTemp);
    setIndividualResume(arrTemp);
    if (item.leaderAdditions !== null) {
      let sweet = JSON.parse(item.leaderAdditions).sweetDataLeader;
      sweet[0] = {
        nameColab: item.nameCollaborator,
        nameLeader: datLead.leaderData.leaderName,
        ...sweet[0],
      };
      setSweetLeader(sweet);
      setBitterLeader(JSON.parse(item.leaderAdditions).bitterDataLeader);
      setShowLeaderAdittions(true);
    } else {
      setShowLeaderAdittions(false);
    }
  };

  const searchColaborators = () => {
    if (boarRequired === "unlimited") {
      getDataColabUnlimited(yearFilter);
    } else {
      getDataCollaborators(period, yearFilter);
    }
  };

  const renderTimestamp = (timestamp) => {
    var date = new Date(timestamp).toLocaleDateString("es-ES");
    console.log("timestampss", date);
    return <strong>{date}</strong>;
  };

  return (
    <>
      <div style={{ textAlign: "center" }}>
        <h1
          style={{
            textAlign: "center",
            fontSize: 58,
            color: "#004F9E",
            fontWeight: "bold",
          }}
          className="resume"
        >
          Historial de Tableros
        </h1>
        <hr />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          width: "80%",
        }}
      >
        <div style={{ width: "24%" }}>
          <h4>Buscar por Colaborador</h4>
          <input
            type="text"
            className="form-control"
            onChange={(e) => getDataSearched(e.target.value)}
          />
          <br />
          <div>
            {dataColaborators.map((item) => (
              <div
                key={item.id.toString()}
                className="cardItems"
                onClick={() => setInfoBoard(item)}
              >
                <strong>{item.nameCollaborator}</strong>
                {item.month !== undefined && (
                  <strong>{renderTimestamp(item.timestamp)}</strong>
                )}
              </div>
            ))}
          </div>
        </div>
        <div style={{ minWidth: "51%" }}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-around",
            }}
          >
            <select
              className="form-control selectStyle"
              style={{ width: "27%", height: 35 }}
              name=""
              id=""
              onChange={(e) => setBoarRequired(e.target.value)}
              defaultValue={period}
            >
              <option value="required">Gestion Desempeño</option>
              <option value="unlimited">Conversación Cotidiana </option>
            </select>
            <select
              className="form-control selectStyle"
              style={{ width: "27%", height: 35 }}
              onChange={(e) => setYearFilter(e.target.value)}
              defaultValue={yearFilter}
            >
              {years.map((year, index) => (
                <option key={index.toString()} value={year}>
                  {year}
                </option>
              ))}
            </select>
            <select
              className="form-control selectStyle"
              style={{ width: "27%", height: 35 }}
              name=""
              id=""
              onChange={(e) => setPeriod(e.target.value)}
              defaultValue={period}
            >
              <option value="1">1</option>
              <option value="2">2</option>
            </select>
            <button
              onClick={() => searchColaborators()}
              className="btn btn-primary"
            >
              <small>Buscar</small>
              <svg
                className="bi bi-search"
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M6.646 3.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L12.293 10 6.646 4.354a.5.5 0 010-.708z"
                />
              </svg>
            </button>
          </div>
          <hr />
          <h5
            style={{
              textAlign: "center",
              color: "#272727",
              fontWeight: "bold",
              marginTop: 20,
            }}
          >
            {nameColab}
          </h5>
          <hr />
          {generalReport.length !== 0 && (
            <ExcelFile
              element={
                <button className="btn btn-success">
                  Descargar Reporte General
                </button>
              }
              filename="Reporte"
            >
              <ExcelSheet data={generalReport} name="Reporte General">
                {generalReport.length !== 0 &&
                  Object.keys(generalReport[0]).map((key, index) => (
                    <ExcelColumn
                      key={index.toString()}
                      label={key}
                      value={key}
                    />
                  ))}
              </ExcelSheet>
            </ExcelFile>
          )}

          {individualResume.length > 0 && (
            <>
              <ExcelFile
                element={
                  <button className="btn btn-success ml-3">
                    Descargar Reporte Individual
                  </button>
                }
                filename="Reporte"
              >
                <ExcelSheet data={individualResume} name="Reporte Individual">
                  {individualResume.length !== 0 &&
                    Object.keys(individualResume[0]).map((key, index) => (
                      <ExcelColumn
                        key={index.toString()}
                        label={key}
                        value={key}
                      />
                    ))}
                </ExcelSheet>
              </ExcelFile>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    width: "99%",
                  }}
                >
                  <div>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        marginTop: 30,
                      }}
                    >
                      <div
                        onClick={() => handleShow("sweet")}
                        className="conversation-box-sweet"
                        style={{
                          textDecoration: "none",
                          borderColor: "transparent",
                        }}
                      >
                        <h4 className="title mt-2 text-white"> LO DULCE</h4>
                        <img src={loDulce} alt="img dulce" />
                      </div>
                    </div>

                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                      }} /* class="d-flex justify-content-around mt-5" */
                    >
                      <div
                        onClick={() => handleShow("bitter")}
                        //to="/conversation/sweet"
                        className="conversation-box-bitter"
                        style={{
                          textDecoration: "none",
                          borderColor: "transparent",
                        }}
                      >
                        <h4 className="title mt-2 text-white">
                          {" "}
                          EXPLOREMOS LO AMARGO
                        </h4>
                        <img className="mt-4" src={LoAmargo} alt="img amargo" />
                      </div>
                    </div>
                  </div>

                  <div
                    style={{
                      display: "flex",
                      marginTop: 30,
                    }} /* class="d-flex justify-content-around mt-5" */
                  >
                    <div
                      onClick={() => handleShow("recognize")}
                      //to="/conversation/sweet"
                      className="conversation-box-weRecognize"
                      style={{
                        textDecoration: "none",
                        borderColor: "transparent",
                      }}
                    >
                      <h4 className="title verticaltext mt-2 text-white">
                        {" "}
                        RECONOCEMOS
                      </h4>
                      <img src={Reconocemos} alt="img reconocemos" />
                    </div>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      marginTop: 30,
                    }}
                  >
                    <div
                      onClick={() => handleShow("additions")}
                      //to="/conversation/sweet"
                      className="conversation-box-additions"
                      style={{
                        textDecoration: "none",
                        borderColor: "transparent",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                      }}
                    >
                      <h4 className="title mt-2 text-white"> ADICIONES</h4>
                      <img src={Adiciones} alt="img adiciones" />
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </div>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {content.map((item) => (
            <div key={item.id.toString()}>
              <p
                style={{
                  border: `1px solid ${getRandomColor()}`,
                  textAlign: "center",
                  borderRadius: 17,
                  backgroundColor: getRandomColor(),
                  color: "white",
                  padding: "5px 0",
                }}
              >
                {item.name}
              </p>
              {title === "ADICIONES" ? (
                <div>
                  <p>Plan de accion:</p>
                  <p>{item.actionPlan}</p>
                  <p>{item.date}</p>
                </div>
              ) : (
                <p></p>
              )}
              {title === "RECONOCEMOS" ? (
                <>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      width: "100%",
                    }}
                  >
                    <img src={item.emoji} alt="" />
                  </div>
                </>
              ) : null}
            </div>
          ))}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleClose}>
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
