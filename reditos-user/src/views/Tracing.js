import React, { useState, useEffect, useCallback } from "react";
import Values from "./Categories/Values";
//import tableIcons from "../constants/MaterialIcons";
//import MaterialTable from "material-table";
import axios from "axios";
import { body, url } from "../constants/auth";
import Objectives from "./Categories/Objectives";
import swal from "sweetalert2";
import CheckList from "./Categories/Checklist";
import SelectedUser from "../components/SelectedUser";
import { useDispatch } from "react-redux";
import {
  getTracingData,
  finishedTracing,
} from "../redux/actions/tracingActions";
import { getSettings } from "../redux/actions/userActions";

export default function Tracing(props) {
  const [currentView, setCurrentView] = useState("category");
  const [infoCat, setInfoCat] = useState([]);
  const [nameCat, setNameCat] = useState("");
  const [isValueComplete, setIsValueComplete] = useState(false);
  const [isObjectivesComplete, setIsObjectivesComplete] = useState(false);
  const [showCheckList, setShowCheckList] = useState(false);
  const [tracingBlocked, setTracingBlocked] = useState(false);
  const [infoValue, setInfoValue] = useState([]);
  const [dataTracing, setDataTracing] = useState([]);
  const [tracingList, setTracingList] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    if (JSON.parse(sessionStorage.getItem("cl")) === true) {
      setShowCheckList(true);
      setCurrentView("");
    } else {
      dispatch(getSettings()).then((item) => {
        var today = new Date();
        let initDate = new Date(item.initDateTracingOne);
        let endDate = new Date(item.endDateTracingOne);
        if (initDate.getTime() < today && today < endDate.getTime()) {
          setTracingBlocked(false);
          let id = sessionStorage.getItem("id");
          if (id !== null) {
            dispatch(
              getTracingData(
                id,
                item.initDateTracingOne,
                item.endDateTracingOne
              )
            ).then((item) => {
              setDataTracing(item);
            });
          } else {
            swal(
              "Oops!",
              "Por favor selecciona un colaborador de la lista para crear o revisar el respectivo seguimiento.",
              "warning"
            ).then(() => {
              props.history.push("/dependents");
            });
          }
        } else {
          setTracingBlocked(true);
        }
      });
    }
  }, [
    showCheckList,
    isValueComplete,
    isObjectivesComplete,
    dispatch,
    props.history,
  ]);

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      console.log("error", error.message);
    }
  };

  const getCategory = useCallback(async () => {
    const token = await getToken();
    fetch(`${url.urlBase}/category/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        setInfoCat(json.results);
      })
      .catch((error) => console.error(error));
  }, []);

  useEffect(() => {
    getCategory();
  }, [getCategory]);

  const finishTracing = () => {
    swal(
      "Advertencia!",
      "Estas seguro de finalizar el segumiento?",
      "warning",
      {
        buttons: {
          cancel: "No",
          defeat: "Si",
        },
      }
    ).then((val) => {
      if (val === "defeat") {
        dispatch(finishedTracing(dataTracing)).then((resp) => {
          if (resp) {
            swal(
              "Genial!",
              "Has finalizado la revisión correctamente.",
              "success"
            ).then(() => {
              props.history.push("/dependents");
            });
          }
        });
      }
    });
  };

  return (
    <>
      {tracingBlocked === false ? (
        JSON.parse(sessionStorage.getItem("cl")) === false ? (
          <div className="container">
            <SelectedUser
              name={sessionStorage.getItem("nameCol")}
              idCol={sessionStorage.getItem("idColab")}
            />
            <br />
            <br />
            {currentView === "category" ? (
              <div className="row">
                <div className="col-md-12">
                  <h2 className="text-center resume">
                    CATEGORÍAS DE EVALUACIÓN
                  </h2>
                </div>
                <div className="col-md-12">
                  <button
                    className="btn btn-info"
                    style={{ margin: "10px 0" }}
                    onClick={finishTracing}
                    type="button"
                    disabled={dataTracing.finished}
                  >
                    Finalizar Revisión
                  </button>
                  {/* <MaterialTable
                    title="CATEGORÍAS DE EVALUACIÓN"
                    icons={tableIcons}
                    columns={[
                      { title: "Categorías", field: "categoryName" },
                      { title: "Peso", field: "weight" },
                    ]}
                    data={infoCat}
                    actions={[
                      {
                        icon: tableIcons.View,
                        tooltip: "Ver",
                        onClick: (event, rowData) => {
                          const result = infoCat.filter(
                            (x) => x.categoryName === rowData.categoryName
                          );
                          setInfoValue(result[0].valuesList);
                          const traceList = dataTracing.tracingItemsList.filter(
                            (el) => el.category === rowData.id
                          );
                          setTracingList(traceList);
                          setNameCat(rowData.categoryName);
                          if (rowData.categoryName === "OBJETIVOS") {
                            setCurrentView("diff");
                          } else {
                            setCurrentView("subcategory");
                          }
                        },
                      },
                    ]}
                    options={{
                      actionsColumnIndex: -1,
                    }}
                    localization={{
                      body: {
                        emptyDataSourceMessage: "No hay registros para mostrar",
                        addTooltip: "Ajouter",
                        deleteTooltip: "Supprimer",
                        editTooltip: "Editer",
                        filterRow: {
                          filterTooltip: "Filtrer",
                        },
                        editRow: {
                          deleteText: "Voulez-vous supprimer cette ligne?",
                          cancelTooltip: "Annuler",
                          saveTooltip: "Enregistrer",
                        },
                      },
                      grouping: {
                        placeholder: "Tirer l'entête ...",
                        groupedBy: "Grouper par:",
                      },
                      header: {
                        actions: "Acciones",
                      },
                      pagination: {
                        labelDisplayedRows: "{from}-{to} de {count}",
                        labelRowsSelect: "lineas",
                        labelRowsPerPage: "lineas por página:",
                        firstAriaLabel: "Primer página",
                        firstTooltip: "Primer página",
                        previousAriaLabel: "Página anterior",
                        previousTooltip: "Página anterior",
                        nextAriaLabel: "Página siguiente",
                        nextTooltip: "Página siguiente",
                        lastAriaLabel: "Última página",
                        lastTooltip: "Última página",
                      },
                      toolbar: {
                        addRemoveColumns: "Ajouter ou supprimer des colonnes",
                        nRowsSelected: "{0} ligne(s) sélectionée(s)",
                        showColumnsTitle: "Voir les colonnes",
                        showColumnsAriaLabel: "Voir les colonnes",
                        searchTooltip: "Buscar",
                        searchPlaceholder: "Buscar",
                      },
                    }}
                  /> */}
                </div>
              </div>
            ) : (
              <div></div>
            )}

            {currentView === "subcategory" ? (
              <Values
                name={nameCat}
                data={infoValue}
                tracing={tracingList}
                finishedTracing={dataTracing.finished}
                isTracing={true}
                customFunctionBack={() => {
                  setCurrentView("category");
                  setNameCat("");
                }}
                customFunctionTracing={() => {
                  swal({
                    title: "¿Está seguro de guardar la evaluación?",
                    icon: "warning",
                    buttons: ["No", "Si"],
                    dangerMode: true,
                  }).then((willDelete) => {
                    if (willDelete) {
                      setCurrentView("category");
                      setNameCat("");
                      setIsValueComplete(true);
                      swal("Registro guardado!", {
                        icon: "success",
                      });
                    } else {
                      swal("Evaluación no guardada");
                    }
                  });
                }}
              />
            ) : (
              <div></div>
            )}

            {nameCat === "OBJETIVOS" ? (
              <Objectives
                name={nameCat}
                isTracing={true}
                customFunctionBack={() => {
                  setCurrentView("category");
                  setNameCat("");
                }}
                customFunction={() => {
                  swal({
                    title: "¿Está seguro de guardar la evaluación?",
                    icon: "warning",
                    buttons: ["No", "Si"],
                    dangerMode: true,
                  }).then((willDelete) => {
                    if (willDelete) {
                      setCurrentView("category");
                      setNameCat("");
                      setIsObjectivesComplete(true);
                      swal("Registro guardado!", {
                        icon: "success",
                      });
                    } else {
                      swal("Evaluación no guardada");
                    }
                  });
                }}
              />
            ) : (
              <div></div>
            )}
          </div>
        ) : (
          <CheckList
            isTracing1={false}
            customFunction={() => {
              swal(
                "Categoría de evaluación",
                "guardada y enviada satisfactoriamente.",
                "success"
              );
            }}
          />
        )
      ) : (
        <h3 className="text-center">
          Los seguimientos para esta fecha se encuentran deshabilitados
        </h3>
      )}
    </>
  );
}
