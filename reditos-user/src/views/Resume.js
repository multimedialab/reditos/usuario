import React, { useState, useEffect, useCallback } from "react";
import { isMobile } from "react-device-detect";
//import MaterialTable from 'material-table';
//import tableIcons from "../constants/MaterialIcons";
import { useDispatch } from "react-redux";
import axios from "axios";
import { body, url } from "../constants/auth";

export default function Resume(props) {
  const [info, setInfo] = useState([]);
  const dispatch = useDispatch();
  const [yearsList, setListYear] = useState([]);
  const [selectedYear, setSelectedYear] = useState("");

  const getToken = async () => {
    try {
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });

      const token = await responseToken.data.token;
      return token;
    } catch (error) {
      alert("error", error.message);
    }
  };

  const consult = useCallback(async () => {
    const token = await getToken();
    fetch(`${url.urlBase}/collaborators/`, {
      headers: { Authorization: `JWT ${token}` },
    })
      .then((response) => response.json())
      .then((json) => {
        setInfo(
          json.results.filter(
            (x) =>
              x.id_leader === JSON.parse(atob(sessionStorage.getItem("uld")))
          )
        );
      })
      .catch((error) => console.error(error));
  }, []);

  const getListYears = () => {
    let currentYear = new Date().getFullYear();
    let tempYears = [currentYear];
    setSelectedYear(currentYear);
    var prevYear = new Date();
    for (let i = 0; i < 6; i++) {
      prevYear.setFullYear(prevYear.getFullYear() - 1);
      let year = prevYear.getFullYear();
      tempYears.push(year);
      setListYear(tempYears);
    }
  };

  useEffect(() => {
    consult();
    getListYears();
  }, [consult, dispatch]);

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-11">
            <div className="text-center">
              <h3 className="pb-4 resume"> RESUMEN DEL AÑO</h3>
            </div>
          </div>
          <div className={isMobile ? "col-12 mb-5" : "col-1"}>
            <select
              className="form-control"
              name=""
              id=""
              onChange={(e) => setSelectedYear(e.target.value)}
              style={{
                width: "150px",
                border: "2px solid #004F9E",
                borderRadius: "5px",
                float: "right",
              }}
            >
              {yearsList.map((year, index) => (
                <option key={index.toString()}>{year}</option>
              ))}
            </select>
          </div>
          <div className="col-12">
            {/* <MaterialTable
              title=""
              icons={tableIcons}
              columns={[
                {
                  title: "Identificador del colaborador",
                  field: "identification",
                },
                { title: "Nombre del colabodor", field: "completeName" },
                {
                  title: "Seguimiento 1",
                  field: "tracingList.length",
                  render: (rowData) =>
                    rowData.tracingList.filter(
                      (el) => el.year === selectedYear && el.period === 1
                    ).length !== 0
                      ? "Realizado"
                      : "No Realizado",
                },
                {
                  title: "Seguimiento 2",
                  field: "tracingList.length",
                  render: (rowData) =>
                    rowData.tracingList.filter(
                      (el) => el.year === selectedYear && el.period === 2
                    ).length !== 0
                      ? "Realizado"
                      : "No Realizado",
                },
              ]}
              data={info}
              localization={{
                body: {
                  emptyDataSourceMessage: "No hay registros para mostrar",
                  addTooltip: "Ajouter",
                  deleteTooltip: "Supprimer",
                  editTooltip: "Editer",
                  filterRow: {
                    filterTooltip: "Filtrer",
                  },
                  editRow: {
                    deleteText: "Voulez-vous supprimer cette ligne?",
                    cancelTooltip: "Annuler",
                    saveTooltip: "Enregistrer",
                  },
                },
                grouping: {
                  placeholder: "Tirer l'entête ...",
                  groupedBy: "Grouper par:",
                },
                header: {
                  actions: "Acciones",
                },
                pagination: {
                  labelDisplayedRows: "{from}-{to} de {count}",
                  labelRowsSelect: "lineas",
                  labelRowsPerPage: "lineas por página:",
                  firstAriaLabel: "Primer página",
                  firstTooltip: "Primer página",
                  previousAriaLabel: "Página anterior",
                  previousTooltip: "Página anterior",
                  nextAriaLabel: "Página siguiente",
                  nextTooltip: "Página siguiente",
                  lastAriaLabel: "Última página",
                  lastTooltip: "Última página",
                },
                toolbar: {
                  addRemoveColumns: "Ajouter ou supprimer des colonnes",
                  nRowsSelected: "{0} ligne(s) sélectionée(s)",
                  showColumnsTitle: "Voir les colonnes",
                  showColumnsAriaLabel: "Voir les colonnes",
                  searchTooltip: "Buscar",
                  searchPlaceholder: "Buscar",
                },
              }}
            /> */}
          </div>
        </div>
      </div>
    </>
  );
}
