import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { consultData } from "../services/consult";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Link } from "react-router-dom";
import { search } from "../services/consult";
import "./styles.css";
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale } from "react-datepicker";
import es from "date-fns/locale/es";
import { Modal} from "react-bootstrap";
import { setAdditionList } from "../redux/actions/boardActions";
registerLocale("es", es);

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */


const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  display: "flex",
  flexDirection: "column",

  justifyContent: "center",
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  border: "2px solid #92225D",
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  boxShadow: "0px 0px 5px 5px #92225D",
  marginBottom: 20,
  textAlign: "center",
  color: "#92225D",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getItemStyle2 = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  display: "flex",
  flexDirection: "column",
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  /* border: "2px solid black", */
  borderRadius: "10px",
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",
  textAlign: "center",
  color: "#92225D",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "transparent",
  /* padding: grid, */
  width: 250,
  /* border: "2px solid black", */
});

const getListStyle2 = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "#92225D",
  padding: grid,
  width: "89%",
  justifyContent: "center",
  borderRadius: 20,
  boxShadow: "0px 0px 5px 5px #92225D",
});

var arrTemp = [];

export default function Additions() {


  


  const stateDataAddition = {
    formationPlan: "",
    formationDate: "",
  };

  const dispatch = useDispatch();
  const [formation, setFormation] = useState(stateDataAddition);
  const [indexState, setIndexState] = useState(0);
  const [itemState, setItemState] = useState({});
  const [arrayComplete, setArrayComplete] = useState([]);
  const [state, setstate] = useState([]);
  const [selected, setSelected] = useState([]);
  const [isSearched, setIsSearched] = useState(false);


  const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);
    setItemState(removed);
  
    destClone.splice(droppableDestination.index, 0, removed);
  
    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;
  
    return result;
  };

  



  useEffect(() => {
    consultData("additions").then((res) => {
      setstate(res);
    });
  }, []);

  const getList = (id) => {
    if (id === "droppable") {
      return state;
    } else {
      return selected;
    }
  };

  const onDragEnd = (result) => {
    const { source, destination } = result;
    console.log('result', result)    
    setIndexState(source.index)    
    // dropped outside the list
    if (!destination) {
      return;
    }
    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        getList(source.droppableId),
        source.index,
        destination.index
      );

      let states = { items };

      if (source.droppableId === "droppable2") {
        states = { selected: items };
        setSelected(states.selected);
        console.log('states selected =>', states.selected);
        
      } else {

        setstate(states.items);
        console.log('states selected else! =>', states.selected);
      }
    } else {
      
      const result = move(
        getList(source.droppableId),
        getList(destination.droppableId),
        source,
        destination
      );
      let aditions = [];
      aditions = result.droppable2;

      console.log('result => ', result);

      
      
      if (source.droppableId === "droppable") {
        handleShow();
      }else{
        
        arrayComplete.splice(source.index,1)
        
        //let i = arrayComplete.indexOf()

      }
      setstate(result.droppable);
      setSelected(result.droppable2);
    }
  };

  const updateCardAddition = () => {

    console.log('itemState', itemState)
    let new_obj = {
      ...itemState,
      actionPlan: formation.formationPlan,
      date: formation.formationDate,
    };
    arrTemp.push(new_obj)
    console.log('arrTemp' , arrTemp)
    setArrayComplete(arrTemp);
    handleClose();
  };

  const searchTarget = async (value) => {
    try {
      await search("additions", value).then((resp) => {
        setstate(resp);
        setIsSearched(true);
      });
    } catch (error) {
      throw Error(error);
    }  
  };

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity

  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const handleChange = (e) => {
    setFormation({
      ...formation,
      [e.target.name]: e.target.value,
    });
  };

  const saveAdditions = () => {
    window.scroll(0, 0);
    console.log('saveadditios', arrayComplete)
    dispatch(setAdditionList(arrayComplete));   
  };

  return (
    <>
      <div
        className="container-fluid"
        style={{ width: "100%", height: "100%", overflowY: "scroll" }}
      >
        <h1
          style={{
            textAlign: "center",
            fontSize: 58,
            color: "#92225D",
            fontWeight: "bold",
          }}
          className="resume"
        >
          Adiciones
        </h1>
        <br />

        <DragDropContext onDragEnd={onDragEnd}>
          <div className="d-flex flex-row">
            <div>
              <input
                onChange={(e) => searchTarget(e.target.value)}
                className="form-control mb-3"
                style={{
                  width: 250,
                  borderColor: "#92225D",
                  borderWidth: 2,
                  borderRadius: 20,
                }}
                placeholder="Buscar"
              />
              <Droppable droppableId="droppable">
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                  >
                    {state.map((item, index) => (
                      <Draggable
                        key={item.id}
                        draggableId={item.id.toString()}
                        index={index}
                      >
                        {(provided, snapshot) => (
                          <>
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={getItemStyle(
                                snapshot.isDragging,
                                provided.draggableProps.style
                              )}
                            >
                              <img
                                style={{ objectFit: "contain" }}
                                src={item.emoji}
                                alt=""
                              />
                              <div
                                style={{
                                  display: "flex",
                                  flexDirection: "column",
                                }}
                              >
                                <h5 className="title"> {item.name} </h5>
                              </div>
                            </div>
                          </>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </div>
            <Droppable droppableId="droppable2">
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  style={getListStyle2(snapshot.isDraggingOver)}
                  className="ml-5"
                >
                  {selected.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id.toString()}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <>
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            className=""
                            style={getItemStyle2(
                              snapshot.isDragging,
                              provided.draggableProps.style
                            )}
                          >
                            <h4 className="title">{item.name}</h4>
                          </div>
                        </>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
        </DragDropContext>
      </div>

      <div className="text-center mt-3">
        <Link to="/conversation/weRecognize">
          <button onClick={() => saveAdditions()} className="btn btn-info">
            <strong>GUARDAR</strong>
          </button>
        </Link>
      </div>

      <Modal backdrop="static" centered show={show} onHide={handleClose}>
        <Modal.Body>
          <div>
            <div className="form-group">
              <h5>Plan de accion</h5>
              <input
                onChange={(e) => handleChange(e)}
                name="formationPlan"
                type="text"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <h5>Fecha</h5>
              <input
                onChange={(e) => handleChange(e)}
                type="date"
                name="formationDate"
                id=""
                className="form-control"
              />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            onClick={() => updateCardAddition()}
            className="btn btn-primary btn-block"
          >
            <h5>
              <strong>Agregar</strong>
            </h5>
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
