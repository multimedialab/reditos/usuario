import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import "./styles.css";
import {
  setPeriodNumber,
  getCollaboratorBoard,
  setTypeBoard,
} from "../redux/actions/boardActions";
import { AiOutlineHistory } from "react-icons/ai";
import { GiCoffeeCup } from "react-icons/gi";
import { BsStopwatch } from "react-icons/bs";
import { IoIosInfinite } from "react-icons/io";
import { GiMuscleUp } from "react-icons/gi";
import { withRouter } from "react-router-dom";
import Swal from "sweetalert2";

function Conversation(props) {
  const dispatch = useDispatch();
  const [period, setPeriod] = useState(0);

  const dateObj = {
    day: "",
    month: "",
    year: "",
  };

  const [date, setDate] = useState(dateObj);
  const [dataUser, setDataUser] = useState({});

  useEffect(() => {
    let dateSystem = new Date();

    let day = dateSystem.getDate();
    let month = dateSystem.getMonth() + 1;
    let year = dateSystem.getFullYear();

    setDate({ day: day, month: month, year: year });

    const data = JSON.parse(atob(sessionStorage.getItem("rdt")));

    setDataUser(data);

    /* let month = 7; */
    if (month >= 1 && month <= 6) {
      setPeriod(1);
      dispatch(setPeriodNumber(1));
    } else {
      setPeriod(2);
      dispatch(setPeriodNumber(2));
    }
  }, [dispatch]);

  const validateCollaboratorBoardRequired = async () => {
    const data = JSON.parse(atob(sessionStorage.getItem("rdt")));
    let id = data.collaboratorData.url.split("/")[5];
    dispatch(getCollaboratorBoard(date.year, period, id)).then((resp) => {
      if (resp.results.length === 0) {
        dispatch(setTypeBoard("REQUIRED"));
        props.history.push("/conversation/sweet");
      } else {
        Swal("Oops!", "Ya ingresaste tu tablero para este periodo");
      }
    });
  };

  const validateCollaboratorBoardUnlimited = async () => {
    dispatch(setTypeBoard("UNLIMITED"));
    props.history.push("/conversation/sweet");
  };

  const validateLeaderBoard = () => {
    Swal.fire({
      title: "<strong>Gestiona tu desempeño</strong>",
      icon: "info",
      showCloseButton: true,
      showDenyButton: true,
      focusConfirm: false,
      confirmButtonText: "Gestion del desempeño",
      confirmButtonAriaLabel: "Thumbs up, great!",
      denyButtonText: "Conversación cotidiana",
    }).then((resp) => {
      if (resp.isConfirmed) {
        validateCollaboratorBoardRequired();
      } else if (resp.isDenied) {
        validateCollaboratorBoardUnlimited();
      }
    });
  };

  return (
    <div className="container">
      <div style={{ textAlign: "center" }}>
        <h1
          style={{
            textAlign: "center",
            fontSize: 58,
            color: "#004F9E",
            fontWeight: "bold",
          }}
          className="resume"
        >
          Seleccionar Opción
        </h1>
        <hr />
      </div>

      <div style={{ display: "flex", justifyContent: "center" }}>
        <h3
          style={{
            textAlign: "center",
            fontSize: 32,
            color: "#004F9E",
            fontWeight: "bold",
          }}
        >
          <small>
            Periodo N° {period} - 01 Enero al 30 Junio del {date.year}
          </small>
        </h3>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100%",
        }}
      >
        {atob(sessionStorage.getItem("ld")) === "true" ? (
          <div
            style={{
              display: "flex",
              marginTop: 30,
            }}
          >
            <Link
              to="/conversation/summary"
              className="conversation-box-4"
              style={{ textDecoration: "none", borderColor: "transparent" }}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                  width: "100%",
                  height: "100%",
                }}
              >
                <h4 className="title mt-2 text-white">
                  <small>HISTORIAL COLABORADORES</small>
                </h4>
                <AiOutlineHistory size={50} color="white" />
              </div>
            </Link>
          </div>
        ) : null}
        <div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: 30,
            }} /* class="d-flex justify-content-around mt-5" */
          >
            {atob(sessionStorage.getItem("ld")) === "true" ? (
              <Link
                /* onClick={() => alert(`Periodo ${period}`)} */
                to="/conversation/selectBoard"
                className="conversation-box"
                style={{ textDecoration: "none", borderColor: "transparent" }}
              >
                <div
                  onClick={() =>
                    sessionStorage.setItem("typeBoard", "REQUIRED")
                  }
                  style={{
                    position: "relative",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <h4 className="title mt-2 text-white">
                    <small>GESTION DEL DESEMPEÑO</small>
                  </h4>
                  <GiCoffeeCup size={50} color="white" />
                  <BsStopwatch
                    size={25}
                    color="white"
                    style={{ position: "absolute", top: 5, right: 10 }}
                  />
                </div>
              </Link>
            ) : (
              <div className="conversation-box" style={{ border: "none" }}>
                <div
                  onClick={() => validateCollaboratorBoardRequired()}
                  style={{
                    position: "relative",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <h4 className="title mt-2 text-white">
                    <small>GESTION DEL DESEMPEÑO</small>
                  </h4>
                  <GiCoffeeCup size={50} color="white" />
                  <BsStopwatch
                    size={25}
                    color="white"
                    style={{ position: "absolute", top: 5, right: 10 }}
                  />
                </div>
              </div>
            )}
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: 30,
            }} /* class="d-flex justify-content-around mt-5" */
          >
            {atob(sessionStorage.getItem("ld")) === "true" ? (
              <Link
                to="/conversation/selectBoard"
                className="conversation-box-2"
                style={{ textDecoration: "none", borderColor: "transparent" }}
              >
                <div
                  onClick={() =>
                    sessionStorage.setItem("typeBoard", "UNLIMITED")
                  }
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <h4 className="title mt-2 text-white">
                    <small>CONVERSACIÓN COTIDIANA</small>
                  </h4>
                  <IoIosInfinite size={50} color="white" />
                </div>
              </Link>
            ) : (
              <Link
                to="/conversation/sweet"
                className="conversation-box-2"
                style={{ textDecoration: "none", borderColor: "transparent" }}
              >
                <div
                  onClick={() => validateCollaboratorBoardUnlimited()}
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <h4 className="title mt-2 text-white">
                    <small>CONVERSACIÓN COTIDIANA</small>
                  </h4>
                  <IoIosInfinite size={50} color="white" />
                </div>
              </Link>
            )}
          </div>
        </div>
        {dataUser.collaboratorData !== null &&
        atob(sessionStorage.getItem("ld")) === "true" ? (
          <>
            <div
              style={{
                display: "flex",
                marginTop: 30,
              }} /* class="d-flex justify-content-around mt-5" */
            >
              <div
                className="conversation-box-3"
                onClick={() => validateLeaderBoard()}
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                  height: "100%",
                }}
              >
                <h4 className="title mt-2 text-white">
                  <small>GESTIONAR MI DESEMPEÑO</small>
                </h4>
                <GiMuscleUp size={50} color="white" />
              </div>
            </div>
          </>
        ) : null}
      </div>
    </div>
  );
}
export default withRouter(Conversation);
