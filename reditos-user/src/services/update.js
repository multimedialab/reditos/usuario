import axios from "axios";
import { url, body } from "../constants/auth";

export const updateInfo = async (name,data,id) => {
    //const token = await apiToken();
    try {
        
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
  
      const token = await responseToken.data.token;
      const response = await axios.put(`${url.urlBase}/${name}/${id}/`, data, {headers: {'Authorization': `JWT ${token}`}});
         
      console.log(response)
      
    } catch (error) {
      console.log("error", error.message);
    }
}


