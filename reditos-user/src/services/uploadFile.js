import axios from 'axios';
import { url, body } from '../constants/auth';


export const uploadSingleImage = async (path) => {

  const formattedPath = new FormData();
  formattedPath.append('file', path);

  try {
    const reponseToken = await axios.post(`${url.urlToken}/api-token-auth/`, body, {
      headers: { 'Content-Type': 'application/json' }
    });
    const token = await reponseToken.data.token;
    const responseFile = await axios.post(`${url.urlBase}/subir/`, formattedPath, { headers: { 'Authorization': `JWT ${token}` } });
    const data = await responseFile.data;
    if (data.length !== 0) {
      return (data);
    }
  } catch (err) {
    // toastr.error(`Lo sentimos, ha sucedido un error, intenta más tarde. ${err.message}`);
  }
}
