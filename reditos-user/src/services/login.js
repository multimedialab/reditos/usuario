import axios from "axios";
import { url, body } from "../constants/auth";
import swal from "sweetalert2";
import { updateInfo } from "../services/update";

export const login = async (email, password) => {
  try {
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;
    const response = await axios.get(
      `${url.urlBase}/accessLogin/?email=${email}&password=${password}`,
      { headers: { Authorization: `JWT ${token}` } }
    );
    const responseMessage = await axios.get(`${url.urlBase}/settings/`, {
      headers: { Authorization: `JWT ${token}` },
    });

    const dataMessage = responseMessage.data.results[0].policyTextCollaborator;

    console.log(response);
    let rps = false;
    if (response.data.count !== 0) {
      switch (response.data.results[0].rol) {
        case 1:
          if (response.data.results[0].leaderData.checkedPolicy) {
            swal("Bienvenido!", "Iniciaste sesion correctamente!", "success");
            rps = true;
          } else {
            swal(
              "AVISO DE PRIVACIDAD PARA TRATAMIENTO DE DATOS",
              `${dataMessage}`,
              {
                closeOnClickOutside: false,
                buttons: {
                  cancel: "Cancelar",
                  catch: {
                    text: "Aceptar",
                    value: "accept",
                  },
                },
              }
            ).then((value) => {
              console.log("value", value);
              switch (value) {
                case "accept":
                  swal(
                    "Información!",
                    "Has aceptado la privacidad de datos correctamente!",
                    "success"
                  );
                  let obj = {
                    id_company: response.data.results[0].leaderData.id_company,
                    id_leader: response.data.results[0].leaderData.url,
                    checkedPolicy: true,
                  };
                  updateInfo(
                    "leaders",
                    obj,
                    response.data.results[0].leaderData.id
                  );
                  rps = true;
                  return true;

                default:
                  swal(
                    "Información!",
                    "No has aceptado la privacidad de datos, contacte con el administrador!",
                    "info"
                  );
                  rps = false;
                  break;
              }
            });
          }
          sessionStorage.setItem(
            "ath",
            btoa(unescape(encodeURIComponent(JSON.stringify(true))))
          );
          sessionStorage.setItem(
            "ld",
            btoa(unescape(encodeURIComponent(JSON.stringify(true))))
          );
          sessionStorage.setItem(
            "rdt",
            btoa(
              unescape(
                encodeURIComponent(JSON.stringify(response.data.results[0]))
              )
            )
          );
          break;
        case 2:
          console.log(
            "response.data.results[0].collaboratorData.checkedPolicy",
            response.data.results[0].collaboratorData.checkedPolicy
          );
          if (response.data.results[0].collaboratorData.checkedPolicy) {
            swal("Bienvenido!", "Iniciaste sesion correctamente!", "success");
            rps = true;
          } else {
            //swal("AVISO DE PRIVACIDAD PARA TRATAMIENTO DE DATOS", `${dataMessage}`, "success")
            swal(
              "AVISO DE PRIVACIDAD PARA TRATAMIENTO DE DATOS",
              `${dataMessage}`,
              {
                closeOnClickOutside: false,
                buttons: {
                  cancel: "Cancelar",
                  catch: {
                    text: "Aceptar",
                    value: "accept",
                  },
                },
              }
            ).then((value) => {
              console.log("value", value);
              switch (value) {
                case "accept":
                  swal(
                    "Información!",
                    "Has aceptado la privacidad de datos correctamente!",
                    "success"
                  );
                  let obj = {
                    id_company:
                      response.data.results[0].collaboratorData.id_company,
                    id_leader:
                      response.data.results[0].collaboratorData.id_leader,
                    checkedPolicy: true,
                  };
                  updateInfo(
                    "collaborators",
                    obj,
                    response.data.results[0].collaboratorData.id
                  );
                  rps = true;
                  return true;

                default:
                  swal(
                    "Información!",
                    "No has aceptado la privacidad de datos, contacte con el administrador!",
                    "info"
                  );
                  rps = false;
                  break;
              }
            });
          }
          sessionStorage.setItem(
            "ath",
            btoa(unescape(encodeURIComponent(JSON.stringify(true))))
          );
          sessionStorage.setItem(
            "ld",
            btoa(unescape(encodeURIComponent(JSON.stringify(false))))
          );
          sessionStorage.setItem(
            "rdt",
            btoa(
              unescape(
                encodeURIComponent(JSON.stringify(response.data.results[0]))
              )
            )
          );

          // window.location.href = "https://reditos.multimedialab.dev/profile";
          break;
        default:
          swal("Error", "Usuario y o contraseña incorrecto", "error");
          break;
      }
    } else {
      swal("Error", "Usuario y o contraseña incorrecto", "error");
    }

    return rps;
  } catch (error) {
    console.log("error", error.message);
  }
};
