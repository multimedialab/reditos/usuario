import axios from 'axios';

export const sendEmails = async (obj) => {
    try {
      await axios.post("https://reditos.multimedialab.dev/Project/s1ils/", obj, {
        headers: { "Content-Type": "application/json" },
      });
    } catch (error) {
      console.log(error.message);
    }
  };