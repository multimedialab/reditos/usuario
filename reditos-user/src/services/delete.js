import axios from "axios";
import { url, body } from "../constants/auth";

export const deleteValue = async (name, id) => {
    //const token = await apiToken();
    try {
        
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
  
      const token = await responseToken.data.token;
  
      const response = await axios.delete(`${url.urlBase}/${name}/${id}/`, {
        headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
      });
      
      console.log(response)
      
    } catch (error) {
      console.log("error", error.message);
    }
}
  
  