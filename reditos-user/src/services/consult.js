import axios from "axios";
import { url, body } from "../constants/auth";

export const consultData = async (name) => {
    //const token = await apiToken();
    try {
        
      const responseToken = await axios.post(url.urlToken, body, {
        headers: { "Content-Type": "application/json" },
      });
  
      const token = await responseToken.data.token;
  
      const response = await axios.get(`${url.urlBase}/${name}/`, {
        headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
      });
      return response.data.results
    } catch (error) {
      console.log("error", error.message);
    }
}

//https://reditos.multimedialab.dev/Project/tracing/?collaborator=51 

export const consultColTracing = async (name,id) => {
  //const token = await apiToken();
  try {      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const response = await axios.get(`${url.urlBase}/${name}/?collaborator=${id}`, {
      headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
    });
    
    
    return response.data.results
  } catch (error) {
    console.log("error", error.message);
  }
}

//Search 

export const search = async (name , value) => {
  //const token = await apiToken();
  try {      
    const responseToken = await axios.post(url.urlToken, body, {
      headers: { "Content-Type": "application/json" },
    });

    const token = await responseToken.data.token;

    const response = await axios.get(`${url.urlBase}/${name}/?search=${value}`, {
      headers: {"Content-Type": "application/json" ,'Authorization': `JWT ${token}`}
    });
    
    
    return response.data.results
  } catch (error) {
    console.log("error", error.message);
  }
}



  
  