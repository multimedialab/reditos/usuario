import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css' ;
import ProtectedRoute from '../src/components/ProtectedRoute';
import Resume from './views/Resume';
import Login from './views/Login';
import ForgetPassword from './views/ForgetPassword';
import Profile from './views/Profile';
import Dependents from './views/Dependents';
import Tracing from './views/Tracing';
import Tracing1 from './views/Tracing1';
import Conversation from './views/Conversation';
import Sweet from './views/Sweet';
import Bitter from './views/Bitter';
import Additions from './views/Additions';
import WeRecognize from './views/WeRecognize';
import SelectBoard from './views/SelectBoard';
import Summary from './views/Summary';
import SweetLeader from './views/SweetLeader';
import BitterLeader from './views/BitterLeader';

function App() {
  return (    
    <Router basename={"/user"}>
        <Switch>  
          <Route exact path="/" component={Login} />        
          <Route exact path="/forget" component={ForgetPassword} /> 
          <ProtectedRoute exact path="/profile" component={Profile} />
          <ProtectedRoute exact path="/dependents" component={Dependents} />
          <ProtectedRoute exact path="/resume" component={Resume} />
          <ProtectedRoute exact path="/tracing" component={Tracing} />
          <ProtectedRoute exact path="/tracing1" component={Tracing1} />
          <ProtectedRoute exact path="/conversation" component={Conversation} />
          <ProtectedRoute exact path="/conversation/sweet" component={Sweet} />
          <ProtectedRoute exact path="/conversation/sweetLeader" component={SweetLeader} />
          <ProtectedRoute exact path="/conversation/bitterLeader" component={BitterLeader} />
          <ProtectedRoute exact path="/conversation/bitter" component={Bitter} />
          <ProtectedRoute exact path="/conversation/additions" component={Additions} />
          <ProtectedRoute exact path="/conversation/weRecognize" component={WeRecognize} />
          <ProtectedRoute exact path="/conversation/selectBoard" component={SelectBoard} />
          <ProtectedRoute exact path="/conversation/summary" component={Summary} />
          <Route path="*" component={() => "404 NOT FOUND"} />
        </Switch>
    </Router>
  );
}

export default App;

